﻿using UnityEngine;

public class FloatingCameraController : MonoBehaviour
{
  public Transform lookAtDefault;
  public Transform lookAtAiming;
  public Transform camTransform;
  // The lower the faster
  public float cameraSmoothness = 0.1f;
  public float camMaxDistance = 15f;

  [HideInInspector]
  public float distance = 15f;
  public float sensitivity = 6f;
  public float minAngleClamp = -30f;
  public float minAngleClampAiming = -30f;
  public float maxAngleClamp = 20f;
  public float maxAngleClampAiming = 60f;
  public LayerMask camOcclusion;
  private PlayerController player;
  private Camera cam;
  private Vector3 cameraVelocity = Vector3.zero;
  private float currentX = 0f;
  private float currentY = 0f;

  void Start()
  {
	distance = camMaxDistance;
    camTransform = transform;
    player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
    cam = Camera.main;
    Cursor.lockState = CursorLockMode.Locked;
    Cursor.visible = false;
  }

  void Update()
  {
    currentX += Input.GetAxis("Mouse X") * sensitivity;
    currentY += Input.GetAxis("Mouse Y") * sensitivity;

    if (player.isAiming)
      currentY = Mathf.Clamp(currentY, minAngleClampAiming, maxAngleClampAiming);
    else
      currentY = Mathf.Clamp(currentY, minAngleClamp, maxAngleClamp);
  }

  void LateUpdate()
  {
    Transform lookAt = lookAtDefault;
    Vector3 dir = new Vector3(0, 0, -distance);
    Quaternion rotation = Quaternion.Euler(-currentY, currentX, 0);
    RaycastHit wallHit = new RaycastHit();

    if (player.isAiming)
    {
      distance = camMaxDistance/2;
      // lookAt.transform.position = new Vector3(3f, lookAt.position.y, lookAt.position.z);
      // lookAt.position = Vector3.MoveTowards(lookAt.position, new Vector3(3f, lookAt.position.y, lookAt.position.z), 100f * Time.deltaTime);
      // lookAt = lookAtDefault;
      // lookAt = lookAtAiming;
      // Vector3 position = lookAt.localPosition;
      // position.x = 3f;
      // lookAt.localPosition = new Vector3(lookAt.localPosition.x, lookAt.localPosition.y, -5f);
      // lookAt = lookAtAiming;
      camTransform.position = Vector3.MoveTowards(camTransform.position, lookAt.position + rotation * dir, 100f * Time.deltaTime);
    }
    else
    {
      distance = camMaxDistance;
      // // // lookAt.localPosition = new Vector3(lookAt.localPosition.x, lookAt.localPosition.y, 0f);
      // camTransform.position = Vector3.MoveTowards(camTransform.position, lookAt.position + rotation * dir, 50f * Time.deltaTime);
      lookAt = lookAtDefault;
      camTransform.position = lookAt.position + rotation * dir;
      // camTransform.position = Vector3.MoveTowards(camTransform.position, lookAt.position + rotation * dir, 100f * Time.deltaTime);
    }

    // camTransform.position = Vector3.Lerp(camTransform.position, lookAt.position + rotation * dir, 500f * Time.deltaTime);
    // camTransform.position = new Vector3(lookAt.position.x + rotation.x * dir.x, lookAt.position.y + rotation.y * dir.y, lookAt.position.z + rotation.z * dir.z);
    // camTransform.position = Vector3.MoveTowards(camTransform.position, lookAt.position + rotation * dir, 500f * Time.deltaTime);
    // camTransform.position = lookAt.position + rotation * dir;
    // camTransform.position = Vector3.smoothDamp(camTransform.position, lookAt.position + rotation * dir, ref cameraVelocity, cameraSmoothness);
    camTransform.LookAt(lookAt.position);

    if (Physics.Linecast(lookAt.position, camTransform.position, out wallHit, camOcclusion))
    {
      if (!(wallHit.rigidbody != null && wallHit.rigidbody.tag == "Player"))
        camTransform.position = new Vector3(wallHit.point.x + wallHit.normal.x * 0.5f, camTransform.position.y, wallHit.point.z + wallHit.normal.z * 0.5f);
    }
  }
}
