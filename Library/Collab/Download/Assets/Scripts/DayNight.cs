﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DayNight : MonoBehaviour {
	//Speed of the cycle (if you set this to 1 the one hour in the cycle will pass in 1 real life second)
	public float daySpeedMultiplier = 0.1f;
	//main directional light
	public Light sunLight;
	public Light moonLight;
	public Camera mainCamera;
	public GameObject cloudSphere;
	//control intensity of sun?
	public bool controlIntensity = true;
	//what's the current time
	[Range(0f, 24.0f)]
	public float currentTime = 0.0f;
	public float sunDirection = -90f;
	public string timeString = "00:00 AM";
	//Set smoothColor transition
	public float smoothColor = 0.1f; 
	//x rotation value of the light
	private float xValueOfSun = 90.0f;

	//Set game objects
	public Material waterMat;
	public Material cloud_1;
	public Material cloud_2;
	public Material skyBoxMat;

	[Header("Sunrise colors")]
	//Set sunrise colors
	[ColorUsageAttribute(true,true)] public Color sunriseColor;
	[ColorUsageAttribute(true,true)] public Color fogSunriseColor;
	[ColorUsageAttribute(true,true)] public Color waterBCSunriseColor;
	[ColorUsageAttribute(true,true)] public Color waterRESunriseColor;
	[ColorUsageAttribute(true,true)] public Color cloud1SunriseColor;
	[ColorUsageAttribute(true,true)] public Color cloud2SunriseColor;
	[ColorUsageAttribute(true,true)] public Color skyBoxTopSunriseColor;
	[ColorUsageAttribute(true,true)] public Color skyBoxHorizonSunriseColor;
	[ColorUsageAttribute(true,true)] public Color skyBoxBottomSunriseColor;

	[Header("Day colors")]
	//Set day colors
	[ColorUsageAttribute(true,true)] public Color dayColor;
	[ColorUsageAttribute(true,true)] public Color fogDayColor;
	[ColorUsageAttribute(true,true)] public Color waterBCDayColor;
	[ColorUsageAttribute(true,true)] public Color waterREDayColor;
	[ColorUsageAttribute(true,true)] public Color cloud1DayColor;
	[ColorUsageAttribute(true,true)] public Color cloud2DayColor;
	[ColorUsageAttribute(true,true)] public Color skyBoxTopDayColor;
	[ColorUsageAttribute(true,true)] public Color skyBoxHorizonDayColor;
	[ColorUsageAttribute(true,true)] public Color skyBoxBottomDayColor;

	[Header("Sunset colors")]
	//Set sunset colors
	[ColorUsageAttribute(true,true)] public Color sunsetColor;
	[ColorUsageAttribute(true,true)] public Color fogSunsetColor;
	[ColorUsageAttribute(true,true)] public Color waterBCSunsetColor;
	[ColorUsageAttribute(true,true)] public Color waterRESunsetColor;
	[ColorUsageAttribute(true,true)] public Color cloud1SunsetColor;
	[ColorUsageAttribute(true,true)] public Color cloud2SunsetColor;
	[ColorUsageAttribute(true,true)] public Color skyBoxTopSunsetColor;
	[ColorUsageAttribute(true,true)] public Color skyBoxHorizonSunsetColor;
	[ColorUsageAttribute(true,true)] public Color skyBoxBottomSunsetColor;

	[Header("Night colors")]
	//Set night colors
	[ColorUsageAttribute(true,true)] public Color nightColor;
	[ColorUsageAttribute(true,true)] public Color fogNightColor;
	[ColorUsageAttribute(true,true)] public Color waterBCNightColor;
	[ColorUsageAttribute(true,true)] public Color waterRENightColor;
	[ColorUsageAttribute(true,true)] public Color cloud1NightColor;
	[ColorUsageAttribute(true,true)] public Color cloud2NightColor;
	[ColorUsageAttribute(true,true)] public Color skyBoxTopNightColor;
	[ColorUsageAttribute(true,true)] public Color skyBoxHorizonNightColor;
	[ColorUsageAttribute(true,true)] public Color skyBoxBottomNightColor;
	
	// Update is called once per frame
	void Update () {
		//increment time
		currentTime += Time.deltaTime*daySpeedMultiplier;
		//reset time
		if (currentTime >= 24.0f) {
			currentTime %= 24.0f;
		}
		//Check for sunlight
		if (sunLight) {
			ControlLight();
		}
	
		//Clouds follow camera
		if (cloudSphere && mainCamera)
		{
			cloudSphere.transform.position = mainCamera.transform.position;
		}

		//Gets The timeString;
		CalculateTime ();
	}
	
	public void SetupLight() {
		//Set color of light
		//Sunrise
		if (currentTime >= 5.0f && currentTime <= 8.0f)
		{
			sunLight.color = sunriseColor;
			RenderSettings.ambientLight = sunriseColor;
			RenderSettings.fogColor = fogSunriseColor;
			waterMat.SetColor("_BaseColor", waterBCSunriseColor);
			waterMat.SetColor("_ReflectionColor",waterRESunriseColor);
			cloud_1.SetColor("_CloudColor", cloud1SunriseColor);
			cloud_2.SetColor("_CloudColor", cloud2SunriseColor);
			skyBoxMat.SetColor("_Color1", skyBoxTopSunriseColor);
			skyBoxMat.SetColor("_Color2", skyBoxHorizonSunriseColor);
			skyBoxMat.SetColor("_Color3", skyBoxBottomSunriseColor);

		}
		//Day
		else if (currentTime >= 8.0f && currentTime <= 16.0f)
		{
			sunLight.color = dayColor;
			RenderSettings.ambientLight = dayColor;
			RenderSettings.fogColor = fogDayColor;
			waterMat.SetColor("_BaseColor", waterBCDayColor);
			waterMat.SetColor("_ReflectionColor",waterREDayColor);
			cloud_1.SetColor("_CloudColor", cloud1DayColor);
			cloud_2.SetColor("_CloudColor", cloud2DayColor);
			skyBoxMat.SetColor("_Color1", skyBoxTopDayColor);
			skyBoxMat.SetColor("_Color2", skyBoxHorizonDayColor);
			skyBoxMat.SetColor("_Color3", skyBoxBottomDayColor);
		}
		//Sunset
		else if (currentTime >= 16.0f && currentTime <= 18.0f)
		{
			sunLight.color = sunsetColor;
			RenderSettings.ambientLight = sunsetColor;
			RenderSettings.fogColor = fogSunsetColor;
			waterMat.SetColor("_BaseColor", waterBCSunsetColor);
			waterMat.SetColor("_ReflectionColor",waterRESunsetColor);
			cloud_1.SetColor("_CloudColor", cloud1SunsetColor);
			cloud_2.SetColor("_CloudColor", cloud2SunsetColor);
			skyBoxMat.SetColor("_Color1", skyBoxTopSunsetColor);
			skyBoxMat.SetColor("_Color2", skyBoxHorizonSunsetColor);
			skyBoxMat.SetColor("_Color3", skyBoxBottomSunsetColor);
		}
		//Night
		else if (currentTime >= 18.0f && currentTime <= 24.0f || currentTime <= 5.0f)
		{
			sunLight.color = nightColor;
			RenderSettings.ambientLight = nightColor;
			RenderSettings.fogColor = fogNightColor;
			waterMat.SetColor("_BaseColor", waterBCNightColor);
			waterMat.SetColor("_ReflectionColor",waterRENightColor);
			cloud_1.SetColor("_CloudColor", cloud1NightColor);
			cloud_2.SetColor("_CloudColor", cloud2NightColor);
			skyBoxMat.SetColor("_Color1", skyBoxTopNightColor);
			skyBoxMat.SetColor("_Color2", skyBoxHorizonNightColor);
			skyBoxMat.SetColor("_Color3", skyBoxBottomNightColor);
		}
	}
	public void ControlLight() {
		//Rotate light
		xValueOfSun = -(90.0f+currentTime*15.0f);
		sunLight.transform.localRotation = Quaternion.Euler((xValueOfSun), sunDirection, 0);
		//reset angle
		if (xValueOfSun >= 360.0f) {
			xValueOfSun = 0.0f;
		}

		//Sunrise
		if (currentTime >= 5.0f && currentTime <= 8.0f)
		{
			sunLight.color = Color.Lerp(sunLight.color, sunriseColor, Time.deltaTime * smoothColor + daySpeedMultiplier);
			RenderSettings.ambientLight = Color.Lerp(RenderSettings.ambientLight, sunriseColor, Time.deltaTime * smoothColor + daySpeedMultiplier);
			RenderSettings.fogColor = Color.Lerp(RenderSettings.fogColor , fogSunriseColor, Time.deltaTime * smoothColor + daySpeedMultiplier);
			waterMat.SetColor("_BaseColor",Color.Lerp(waterMat.GetColor("_BaseColor"), waterBCSunriseColor, Time.deltaTime * smoothColor + daySpeedMultiplier));
			waterMat.SetColor("_ReflectionColor",Color.Lerp(waterMat.GetColor("_ReflectionColor"), waterRESunriseColor, Time.deltaTime * smoothColor + daySpeedMultiplier));
			cloud_1.SetColor("_CloudColor",Color.Lerp(cloud_1.GetColor("_CloudColor"), cloud1SunriseColor, Time.deltaTime * smoothColor + daySpeedMultiplier));
			cloud_2.SetColor("_CloudColor",Color.Lerp(cloud_2.GetColor("_CloudColor"), cloud2SunriseColor, Time.deltaTime * smoothColor + daySpeedMultiplier));
			skyBoxMat.SetColor("_Color1",Color.Lerp(skyBoxMat.GetColor("_Color1"), skyBoxTopSunriseColor, Time.deltaTime * smoothColor + daySpeedMultiplier));
			skyBoxMat.SetColor("_Color2",Color.Lerp(skyBoxMat.GetColor("_Color2"), skyBoxHorizonSunriseColor, Time.deltaTime * smoothColor + daySpeedMultiplier));
			skyBoxMat.SetColor("_Color3",Color.Lerp(skyBoxMat.GetColor("_Color3"), skyBoxBottomSunriseColor, Time.deltaTime * smoothColor + daySpeedMultiplier));
		}
		//Day
		else if (currentTime >= 8.0f && currentTime <= 16.0f)
		{
			sunLight.color = Color.Lerp(sunLight.color, dayColor, Time.deltaTime * smoothColor + daySpeedMultiplier);
			RenderSettings.ambientLight = Color.Lerp(RenderSettings.ambientLight, dayColor, Time.deltaTime * smoothColor + daySpeedMultiplier);
			RenderSettings.fogColor = Color.Lerp(RenderSettings.fogColor , fogDayColor, Time.deltaTime * smoothColor + daySpeedMultiplier);
			waterMat.SetColor("_BaseColor",Color.Lerp(waterMat.GetColor("_BaseColor"), waterBCDayColor, Time.deltaTime * smoothColor + daySpeedMultiplier));
			waterMat.SetColor("_ReflectionColor",Color.Lerp(waterMat.GetColor("_ReflectionColor"), waterREDayColor, Time.deltaTime * smoothColor + daySpeedMultiplier));
			cloud_1.SetColor("_CloudColor",Color.Lerp(cloud_1.GetColor("_CloudColor"), cloud1DayColor, Time.deltaTime * smoothColor + daySpeedMultiplier));
			cloud_2.SetColor("_CloudColor",Color.Lerp(cloud_2.GetColor("_CloudColor"), cloud2DayColor, Time.deltaTime * smoothColor + daySpeedMultiplier));
			skyBoxMat.SetColor("_Color1",Color.Lerp(skyBoxMat.GetColor("_Color1"), skyBoxTopDayColor, Time.deltaTime * smoothColor + daySpeedMultiplier));
			skyBoxMat.SetColor("_Color2",Color.Lerp(skyBoxMat.GetColor("_Color2"), skyBoxHorizonDayColor, Time.deltaTime * smoothColor + daySpeedMultiplier));
			skyBoxMat.SetColor("_Color3",Color.Lerp(skyBoxMat.GetColor("_Color3"), skyBoxBottomDayColor, Time.deltaTime * smoothColor + daySpeedMultiplier));
		}
		//Sunset
		else if (currentTime >= 16.0f && currentTime <= 18.0f)
		{
			sunLight.color = Color.Lerp(sunLight.color, sunsetColor, Time.deltaTime * smoothColor + daySpeedMultiplier);
			RenderSettings.ambientLight = Color.Lerp(RenderSettings.ambientLight, sunsetColor, Time.deltaTime * smoothColor + daySpeedMultiplier);
			RenderSettings.fogColor = Color.Lerp(RenderSettings.fogColor , fogSunsetColor, Time.deltaTime * smoothColor + daySpeedMultiplier);
			waterMat.SetColor("_BaseColor",Color.Lerp(waterMat.GetColor("_BaseColor"), waterBCSunsetColor, Time.deltaTime * smoothColor + daySpeedMultiplier));	
			waterMat.SetColor("_ReflectionColor",Color.Lerp(waterMat.GetColor("_ReflectionColor"), waterRESunsetColor, Time.deltaTime * smoothColor + daySpeedMultiplier));
			cloud_1.SetColor("_CloudColor",Color.Lerp(cloud_1.GetColor("_CloudColor"), cloud1SunsetColor, Time.deltaTime * smoothColor + daySpeedMultiplier));
			cloud_2.SetColor("_CloudColor",Color.Lerp(cloud_2.GetColor("_CloudColor"), cloud2SunsetColor, Time.deltaTime * smoothColor + daySpeedMultiplier));
			skyBoxMat.SetColor("_Color1",Color.Lerp(skyBoxMat.GetColor("_Color1"), skyBoxTopSunsetColor, Time.deltaTime * smoothColor + daySpeedMultiplier));
			skyBoxMat.SetColor("_Color2",Color.Lerp(skyBoxMat.GetColor("_Color2"), skyBoxHorizonSunsetColor, Time.deltaTime * smoothColor + daySpeedMultiplier));
			skyBoxMat.SetColor("_Color3",Color.Lerp(skyBoxMat.GetColor("_Color3"), skyBoxBottomSunsetColor, Time.deltaTime * smoothColor + daySpeedMultiplier));
		}
		//Night
		else if (currentTime >= 18.0f && currentTime <= 24.0f || currentTime <= 5.0f)
		{
			sunLight.color = Color.Lerp(sunLight.color, nightColor, Time.deltaTime * smoothColor + daySpeedMultiplier);
			RenderSettings.ambientLight = Color.Lerp(RenderSettings.ambientLight, nightColor, Time.deltaTime * smoothColor + daySpeedMultiplier);
			RenderSettings.fogColor = Color.Lerp(RenderSettings.fogColor , fogNightColor, Time.deltaTime * smoothColor + daySpeedMultiplier);
			waterMat.SetColor("_BaseColor",Color.Lerp(waterMat.GetColor("_BaseColor"), waterBCNightColor, Time.deltaTime * smoothColor + daySpeedMultiplier));
			waterMat.SetColor("_ReflectionColor",Color.Lerp(waterMat.GetColor("_ReflectionColor"), waterRENightColor, Time.deltaTime + daySpeedMultiplier));
			cloud_1.SetColor("_CloudColor",Color.Lerp(cloud_1.GetColor("_CloudColor"), cloud1NightColor, Time.deltaTime * smoothColor + daySpeedMultiplier));
			cloud_2.SetColor("_CloudColor",Color.Lerp(cloud_2.GetColor("_CloudColor"), cloud2NightColor, Time.deltaTime * smoothColor + daySpeedMultiplier));
			skyBoxMat.SetColor("_Color1",Color.Lerp(skyBoxMat.GetColor("_Color1"), skyBoxTopNightColor, Time.deltaTime * smoothColor + daySpeedMultiplier));
			skyBoxMat.SetColor("_Color2",Color.Lerp(skyBoxMat.GetColor("_Color2"), skyBoxHorizonNightColor, Time.deltaTime * smoothColor + daySpeedMultiplier));
			skyBoxMat.SetColor("_Color3",Color.Lerp(skyBoxMat.GetColor("_Color3"), skyBoxBottomNightColor, Time.deltaTime * smoothColor + daySpeedMultiplier));
		}
		//This basically turn on and off the sun light based on day / night
		if (controlIntensity && sunLight && (currentTime >= 18.0f || currentTime <= 5.0f)) {
			RenderSettings.reflectionIntensity = Mathf.MoveTowards(RenderSettings.reflectionIntensity,0.0f,Time.deltaTime*daySpeedMultiplier*10.0f);
			sunLight.intensity = Mathf.MoveTowards(sunLight.intensity,0.0f,Time.deltaTime*50.0f);
			moonLight.intensity = Mathf.MoveTowards(moonLight.intensity,1.0f,Time.deltaTime*50.0f);

		} else if (controlIntensity && sunLight) {
			RenderSettings.reflectionIntensity = Mathf.MoveTowards(RenderSettings.reflectionIntensity,1.0f,Time.deltaTime*daySpeedMultiplier*10.0f);
			sunLight.intensity = Mathf.MoveTowards(sunLight.intensity,7.0f,Time.deltaTime*50.0f);
			moonLight.intensity = Mathf.MoveTowards(moonLight.intensity,0.0f,Time.deltaTime*50.0f);
		}

	}
	public void CalculateTime() {
		//Is it am of pm?
		string AMPM = "";
		float minutes = ((currentTime) - (Mathf.Floor(currentTime)))*60.0f;
		if (currentTime <= 12.0f) {
			AMPM = "AM";

		} else {
			AMPM = "PM";
		}
		//Make the final string
		timeString = Mathf.Floor(currentTime).ToString() + " : " + minutes.ToString("F0") + " "+AMPM ;

	}

}
