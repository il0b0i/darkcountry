﻿using UnityEngine;

public class ShootingController_ : MonoBehaviour
{
  public float gunRange = 100f;
  public float gunDamage = 40f;
  public int leftGunBullets = 6;
  public int rightGunBullets = 6;
  public float shootSpeed = 10f;
  public float reloadSpeed = 20f;
  public int maxReflections = 2;
  //
  public Color lineRenderColorBase;
  public Color lineRenderColorTarget;
  public Material lineRenderLeftMaterial;
  public Material lineRenderRightMaterial;
  public int nReflections = 0;
  //
  public float nextShoot = 2f;
  private bool canShootLeftGun = false;
  private bool canShootRightGun = true;
  public bool isReloading = false;
  public bool isShootingLeft = false;
  public bool isShootingRight = false;
  private float currReloadTime = 0f;
  private int maxBullets = 0;
  private bool canAim = false;
  //
  private Ray ray;
  private RaycastHit hit;
  private Vector3 nDirection;

  //
  public GameObject bulletLeftOrigin;
  public GameObject bulletRightOrigin;
  public ParticleSystem muzzleFlashLeft;
  public ParticleSystem muzzleFlashRight;
  public ParticleSystem bulletTrailLeft;
  public ParticleSystem bulletTrailRight;
  public AudioClip gunShotSound;
  public AudioClip reloadSound;
  //
  private PlayerController player;
  private LineRenderer lineRenderLeft;
  private LineRenderer lineRenderRight;

  //ObjectPooler objectPooler;
  //AURA ESTUVO AQUÍ. MUAJAJAJAJAJAJAJAJAJAJAJA...

  void Start()
  {
	//objectPooler = ObjectPooler.Instance;
    player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
	currReloadTime = reloadSpeed;
	maxBullets = leftGunBullets + rightGunBullets;
	lineRenderLeft = bulletLeftOrigin.GetComponent<LineRenderer>();
	lineRenderRight = bulletRightOrigin.GetComponent<LineRenderer>();
	lineRenderLeft.SetWidth(0.1f,0.1f);
	lineRenderRight.SetWidth(0.1f,0.1f);
	lineRenderLeftMaterial.SetColor("_EmissionColor", lineRenderColorBase); 
	lineRenderRightMaterial.SetColor("_EmissionColor", lineRenderColorBase); 
  }

  private void unHolstered()
  {
	if (!canAim) 
	{
		canAim = true; 
	}
	else canAim = false;
  }


  void Update()
  {
	if (player.isAiming && canAim)
    {	
		AimLineLeft();
		AimLineRight();
		if (Input.GetButton("Fire1") && canShootLeftGun && leftGunBullets > 0)
		{
	  		ShootLeft();
		}
		else if (Input.GetButton("Fire1") && canShootRightGun && rightGunBullets > 0)
		{
			ShootRight();
		}
	}
	else
	{
		lineRenderLeft.enabled = false;
		lineRenderRight.enabled = false;
	}
	//Reset next shoot
	if (isShootingLeft || isShootingRight) 
	{
		nextShoot -= shootSpeed * Time.deltaTime;
	}
		
	if (nextShoot <= 0f)
	{
		if (isShootingRight)
		{
			canShootLeftGun = true; nextShoot = 2f; isShootingRight = false;
		}
		else if (isShootingLeft)
		{
			canShootRightGun = true; nextShoot = 2f; isShootingLeft = false;
		}
	}

	//Reload guns
	if (rightGunBullets <= 0 && leftGunBullets <= 0 && !isShootingLeft && !isShootingRight)
	{
		currReloadTime -= 10 * Time.deltaTime;
		if (isReloading == false)
		{
			AudioSource gunRightAudioSource = bulletRightOrigin.GetComponent<AudioSource>();
			gunRightAudioSource.clip = reloadSound;
			gunRightAudioSource.Play();
			isReloading = true;
		}
		if(currReloadTime <= 0)
		{
			Reload();
		}
	}
  }

  //Shoot left gun
  void ShootLeft()
  {
	AudioSource gunLeftAudioSource = bulletLeftOrigin.GetComponent<AudioSource>();
	gunLeftAudioSource.clip = gunShotSound;
	gunLeftAudioSource.Play();
	Animator animator = GameObject.FindGameObjectWithTag("Player").GetComponent<Animator>();
	animator.Play("shooting",3 , (1f/20)*1);
	isShootingLeft = true;
	canShootRightGun = false;
	canShootLeftGun = false;
	leftGunBullets -= 1;
	muzzleFlashLeft.Play();
	bulletTrailLeft.Play();
	ray = new Ray(bulletLeftOrigin.transform.position, bulletLeftOrigin.transform.forward);
	Debug.DrawRay(ray.origin, ray.direction * 100, Color.green, 5, false);
	//If first shot
    for(int i=0; i<=maxReflections; i++)
	{
		if(i==0)  
			{  
				if (Physics.Raycast(ray.origin, ray.direction, out hit, gunRange))
				{
					nDirection = Vector3.Reflect(ray.direction, hit.normal);
					ray = new Ray(hit.point, nDirection);
					RayTarget target = hit.transform.GetComponent<RayTarget>();
					if (target != null) 
					{
						Debug.Log(hit.transform.name);
						target.isHit = true;
						target.TakeDamage(gunDamage);
					}
				Debug.DrawRay(hit.point, hit.normal*3, Color.magenta, 5, false);  
				Debug.DrawRay(ray.origin, ray.direction * gunRange, Color.red, 5, false);
				}
			}
		else 
		{
			//If at least 1 reflections
			if (Physics.Raycast(ray.origin, ray.direction, out hit, gunRange))
			{
				nDirection = Vector3.Reflect(ray.direction, hit.normal);
				ray = new Ray(hit.point, nDirection);
				RayTarget target = hit.transform.GetComponent<RayTarget>();
				if (target != null) 
				{
					Debug.Log(hit.transform.name);
					target.isHit = true;
					target.TakeDamage(gunDamage/i);
				}

				Debug.DrawRay(hit.point, hit.normal*3, Color.magenta, 5, false);  
				Debug.DrawRay(ray.origin, ray.direction * gunRange, Color.blue, 5, false);
			}
		}
	}
  }

  //Shoot right gun
  void ShootRight()
  {
	AudioSource gunRightAudioSource = bulletRightOrigin.GetComponent<AudioSource>();
	gunRightAudioSource.clip = gunShotSound;
	gunRightAudioSource.Play();
	Animator animator = GameObject.FindGameObjectWithTag("Player").GetComponent<Animator>();
	animator.Play("shooting",3 , (1f/20)*10);
  	isShootingRight = true;
	canShootLeftGun = false;
	canShootRightGun = false;
	rightGunBullets -= 1;
	muzzleFlashRight.Play();
	bulletTrailRight.Play();
	ray = new Ray(bulletRightOrigin.transform.position, bulletRightOrigin.transform.forward);
	Debug.DrawRay(ray.origin, ray.direction * gunRange, Color.green, 5, false);
	//If first shot
    for(int i=0; i<=maxReflections; i++)
	{
		if(i==0)  
			{  
				if (Physics.Raycast(ray.origin, ray.direction, out hit, gunRange))
				{
					nDirection = Vector3.Reflect(ray.direction, hit.normal);
					ray = new Ray(hit.point, nDirection);
					RayTarget target = hit.transform.GetComponent<RayTarget>();
					if (target != null) 
					{
						Debug.Log(hit.transform.name);
						target.isHit = true;
						target.TakeDamage(gunDamage);
					}

				Debug.DrawRay(hit.point, hit.normal*3, Color.magenta, 5, false);  
				Debug.DrawRay(ray.origin, ray.direction * gunRange, Color.red, 5, false);
				}
		}
		else 
		{
			//If at least 1 reflections
			if (Physics.Raycast(ray.origin, ray.direction, out hit, gunRange))
			{
				nDirection = Vector3.Reflect(ray.direction, hit.normal);
				ray = new Ray(hit.point, nDirection);
				RayTarget target = hit.transform.GetComponent<RayTarget>();
				if (target != null) 
				{
					Debug.Log(hit.transform.name);
					target.isHit = true;
					target.TakeDamage(gunDamage/i);
				}
			
				Debug.DrawRay(hit.point, hit.normal*3, Color.magenta, 5, false);  
				Debug.DrawRay(ray.origin, ray.direction * gunRange, Color.blue, 5, false);
			}
		}
	}

  }

  void Reload()
  {
	isReloading = false;
	currReloadTime = reloadSpeed;
	leftGunBullets = (maxBullets/2);
	rightGunBullets = (maxBullets/2);
  }

  void AimLineLeft()
  {
	lineRenderLeftMaterial.SetColor("_EmissionColor", lineRenderColorBase); 
	nReflections = maxReflections;
	//Left Aim Line
  	ray = new Ray(bulletLeftOrigin.transform.position, bulletLeftOrigin.transform.forward);
	lineRenderLeft.positionCount = nReflections;
	lineRenderLeft.SetPosition(0, ray.origin);
	lineRenderLeft.SetPosition(1, ray.GetPoint(gunRange));
	//If first shot
    for(int i=0; i<=maxReflections; i++)
	{
		if(i==0)  
		{  
				if (Physics.Raycast(ray.origin, ray.direction, out hit, gunRange))
				{
					nDirection = Vector3.Reflect(ray.direction, hit.normal);
					ray = new Ray(hit.point, nDirection);
				
					//RayTarget target = hit.transform.GetComponent<RayTarget>();
					//if (target != null) 
					//{
					//	Debug.Log("SHOOT!");
					//}
					lineRenderLeft.enabled = true;
					lineRenderLeft.positionCount = ++nReflections;
					lineRenderLeft.SetPosition(i+1, hit.point);
					lineRenderLeft.SetPosition(nReflections - 1,  ray.GetPoint(gunRange/4));
				}
				else
				{
					lineRenderLeft.enabled = false;
				}
		}
		//If at least 1 reflections
		else
		{
			if (Physics.Raycast(ray.origin, ray.direction, out hit, gunRange))
			{
				nDirection = Vector3.Reflect(ray.direction, hit.normal);
				ray = new Ray(hit.point, nDirection);
				RayTarget target = hit.transform.GetComponent<RayTarget>();
				if (target != null) 
				{
					lineRenderLeftMaterial.SetColor("_EmissionColor", lineRenderColorTarget); 
					//Debug.Log("SHOOT LEFT!");
				}

				lineRenderLeft.positionCount = ++nReflections ;
				lineRenderLeft.SetPosition(i+1, hit.point);
				lineRenderLeft.SetPosition(nReflections - 1,  ray.GetPoint(gunRange/4));
			}
		}
	}
  }

    void AimLineRight()
  {
	lineRenderRightMaterial.SetColor("_EmissionColor", lineRenderColorBase); 
	nReflections = maxReflections;
	//Left Aim Line
  	ray = new Ray(bulletRightOrigin.transform.position, bulletRightOrigin.transform.forward);
	lineRenderRight.positionCount = nReflections;
	lineRenderRight.SetPosition(0, ray.origin);
	lineRenderRight.SetPosition(1, ray.GetPoint(gunRange));
	//If first shot
    for(int i=0; i<=maxReflections; i++)
	{
		if(i==0)  
		{  
				if (Physics.Raycast(ray.origin, ray.direction, out hit, gunRange))
				{
					nDirection = Vector3.Reflect(ray.direction, hit.normal);
					ray = new Ray(hit.point, nDirection);
					//RayTarget target = hit.transform.GetComponent<RayTarget>();
					//if (target != null) 
					//{
					//	Debug.Log("SHOOT!");
					//}

					lineRenderRight.enabled = true;
					lineRenderRight.positionCount = ++nReflections;
					lineRenderRight.SetPosition(i+1, hit.point);
					lineRenderRight.SetPosition(nReflections - 1,  ray.GetPoint(gunRange/4));
				}
				else
				{
					lineRenderRight.enabled = false;
				}
		}
		//If at least 1 reflections
		else
		{
			if (Physics.Raycast(ray.origin, ray.direction, out hit, gunRange))
			{
				nDirection = Vector3.Reflect(ray.direction, hit.normal);
				ray = new Ray(hit.point, nDirection);
				RayTarget target = hit.transform.GetComponent<RayTarget>();
				if (target != null) 
				{
					lineRenderRightMaterial.SetColor("_EmissionColor", lineRenderColorTarget); 
					//Debug.Log("SHOOT RIGHT!");
				}

				lineRenderRight.positionCount = ++nReflections ;
				lineRenderRight.SetPosition(i+1, hit.point);
				lineRenderRight.SetPosition(nReflections - 1,  ray.GetPoint(gunRange/4));
			}
		}
	}
  }
}