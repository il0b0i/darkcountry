﻿using UnityEngine;

public class PlayerController : MonoBehaviour
{
  public float speed = 6f;
  public float runMultiplier = 3.3f;
  public bool isWalking = false;
  public bool isRunning = false;
  public Camera floatingCamera;
  private Rigidbody rigidBody;
  private Animator playerAnimator;
  private const float ROTATION_SPEED = 8f;


  void Start()
  {
    rigidBody = GetComponent<Rigidbody>();
    playerAnimator = GetComponent<Animator>();
    // floatingCamera = GetComponent<Camera>();
  }

  void Update()
  {
    float playerSpeed = speed;
    Vector3 cameraForward = floatingCamera.transform.forward;
    Quaternion cameraRotation = floatingCamera.transform.rotation;

    // Handle running toggle
    if (Input.GetKey(KeyCode.LeftShift))
      playerSpeed *= runMultiplier;
    else
      playerSpeed = speed;

    // Handle forward motion movement
    if (Input.GetKey(KeyCode.W))
    {
      rigidBody.position += new Vector3(cameraForward.x, 0, cameraForward.z) * Time.deltaTime * playerSpeed;
      rigidBody.rotation = Quaternion.Slerp(transform.rotation, RotateY(cameraRotation.eulerAngles.y), ROTATION_SPEED * Time.deltaTime);
    }
    else if (Input.GetKey(KeyCode.S))
    {
      rigidBody.position -= new Vector3(cameraForward.x, 0, cameraForward.z) * Time.deltaTime * playerSpeed;
      rigidBody.rotation = Quaternion.Slerp(transform.rotation, RotateY(cameraRotation.eulerAngles.y - 180), ROTATION_SPEED * Time.deltaTime);
    }

    // Handle rightward motion movement
    if (Input.GetKey(KeyCode.A))
    {
      rigidBody.position -= new Vector3(floatingCamera.transform.right.x, 0, floatingCamera.transform.right.z) * Time.deltaTime * playerSpeed;

      if (Input.GetKey(KeyCode.W))
        rigidBody.rotation = Quaternion.Slerp(transform.rotation, RotateY(cameraRotation.eulerAngles.y - 45), ROTATION_SPEED * Time.deltaTime);
      else if (Input.GetKey(KeyCode.S))
        rigidBody.rotation = Quaternion.Slerp(transform.rotation, RotateY(cameraRotation.eulerAngles.y + 225), ROTATION_SPEED * Time.deltaTime);
      else
        rigidBody.rotation = Quaternion.Slerp(transform.rotation, RotateY(cameraRotation.eulerAngles.y + 270), ROTATION_SPEED * Time.deltaTime);
    }
    else if (Input.GetKey(KeyCode.D))
    {
      rigidBody.position += new Vector3(floatingCamera.transform.right.x, 0, floatingCamera.transform.right.z) * Time.deltaTime * playerSpeed;

      if (Input.GetKey(KeyCode.W))
        rigidBody.rotation = Quaternion.Slerp(transform.rotation, RotateY(cameraRotation.eulerAngles.y + 45), ROTATION_SPEED * Time.deltaTime);
      else if (Input.GetKey(KeyCode.S))
        rigidBody.rotation = Quaternion.Slerp(transform.rotation, RotateY(cameraRotation.eulerAngles.y - 225), ROTATION_SPEED * Time.deltaTime);
      else
        rigidBody.rotation = Quaternion.Slerp(transform.rotation, RotateY(cameraRotation.eulerAngles.y + 90), ROTATION_SPEED * Time.deltaTime);
    }

    if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D))
    {
      isWalking = true;

      if (Input.GetKey(KeyCode.LeftShift))
        isRunning = true;
      else
        isRunning = false;
    }
    else
    {
      isWalking = false;
      isRunning = false;
    }

    if (isWalking)
      playerAnimator.SetBool("isWalking", true);
    else
      playerAnimator.SetBool("isWalking", false);

    if (isRunning)
      playerAnimator.SetBool("isRunning", true);
    else
      playerAnimator.SetBool("isRunning", false);
  }

  Quaternion RotateY(float degrees)
  {
    float xRotation = floatingCamera.transform.rotation.x;
    float zRotation = floatingCamera.transform.rotation.z;

    Quaternion correctedCameraRotation = Quaternion.Euler(xRotation, 0, zRotation);
    return correctedCameraRotation * Quaternion.Euler(0, degrees, 0);
  }
}
