%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_PrefabParentObject: {fileID: 0}
  m_PrefabInternal: {fileID: 0}
  m_Name: PlayerMask
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Armature
    m_Weight: 1
  - m_Path: Armature/hip
    m_Weight: 1
  - m_Path: Armature/hip/abs
    m_Weight: 1
  - m_Path: Armature/hip/abs/chest
    m_Weight: 1
  - m_Path: Armature/hip/abs/chest/coatpull1_l
    m_Weight: 1
  - m_Path: Armature/hip/abs/chest/coatpull1_l/coatpull1_l_001
    m_Weight: 1
  - m_Path: Armature/hip/abs/chest/coatpull1_l/coatpull1_l_001/coatpull1_l_002
    m_Weight: 1
  - m_Path: Armature/hip/abs/chest/coatpull1_l_003
    m_Weight: 1
  - m_Path: Armature/hip/abs/chest/coatpull1_l_003/coatpull1_l_004
    m_Weight: 1
  - m_Path: Armature/hip/abs/chest/coatpull1_l_003/coatpull1_l_004/coatpull1_l_005
    m_Weight: 1
  - m_Path: Armature/hip/abs/chest/coatpull1_r
    m_Weight: 1
  - m_Path: Armature/hip/abs/chest/coatpull1_r/coatpull1_r_001
    m_Weight: 1
  - m_Path: Armature/hip/abs/chest/coatpull1_r/coatpull1_r_001/coatpull1_r_002
    m_Weight: 1
  - m_Path: Armature/hip/abs/chest/coatpull1_r_003
    m_Weight: 1
  - m_Path: Armature/hip/abs/chest/coatpull1_r_003/coatpull1_r_004
    m_Weight: 1
  - m_Path: Armature/hip/abs/chest/coatpull1_r_003/coatpull1_r_004/coatpull1_r_005
    m_Weight: 1
  - m_Path: Armature/hip/abs/chest/neck
    m_Weight: 1
  - m_Path: Armature/hip/abs/chest/neck/head
    m_Weight: 1
  - m_Path: Armature/hip/abs/chest/shoulder_l
    m_Weight: 1
  - m_Path: Armature/hip/abs/chest/shoulder_l/upperarm_l
    m_Weight: 1
  - m_Path: Armature/hip/abs/chest/shoulder_l/upperarm_l/lowerarm_l
    m_Weight: 1
  - m_Path: Armature/hip/abs/chest/shoulder_l/upperarm_l/lowerarm_l/hand_l
    m_Weight: 1
  - m_Path: Armature/hip/abs/chest/shoulder_r
    m_Weight: 1
  - m_Path: Armature/hip/abs/chest/shoulder_r/upperarm_r
    m_Weight: 1
  - m_Path: Armature/hip/abs/chest/shoulder_r/upperarm_r/lowerarm_r
    m_Weight: 1
  - m_Path: Armature/hip/abs/chest/shoulder_r/upperarm_r/lowerarm_r/hand_r
    m_Weight: 1
  - m_Path: Armature/hip/upperleg_l
    m_Weight: 1
  - m_Path: Armature/hip/upperleg_l/lowerleg_l
    m_Weight: 1
  - m_Path: Armature/hip/upperleg_l/lowerleg_l/ankle_l
    m_Weight: 1
  - m_Path: Armature/hip/upperleg_l/lowerleg_l/ankle_l/foot_l
    m_Weight: 1
  - m_Path: Armature/hip/upperleg_r
    m_Weight: 1
  - m_Path: Armature/hip/upperleg_r/lowerleg_r
    m_Weight: 1
  - m_Path: Armature/hip/upperleg_r/lowerleg_r/ankle_r
    m_Weight: 1
  - m_Path: Armature/hip/upperleg_r/lowerleg_r/ankle_r/foot_r
    m_Weight: 1
  - m_Path: coat
    m_Weight: 0
  - m_Path: feet_straps
    m_Weight: 0
  - m_Path: gun_left
    m_Weight: 0
  - m_Path: gun_right
    m_Weight: 0
  - m_Path: hair
    m_Weight: 0
  - m_Path: hat
    m_Weight: 0
  - m_Path: hip_strap
    m_Weight: 0
  - m_Path: leg_starps
    m_Weight: 0
  - m_Path: man
    m_Weight: 0
  - m_Path: vest
    m_Weight: 0
