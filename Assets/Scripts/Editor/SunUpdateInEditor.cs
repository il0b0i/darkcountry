﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(DayNight))]
public class SunUpdateInEditor : Editor {

	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI();

		DayNight script = (DayNight)target;
  
		if(GUILayout.Button("Update light"))
		{
			script.CalculateTime();
			script.SetupLight();
			script.ControlLight();
		}
	}
}
