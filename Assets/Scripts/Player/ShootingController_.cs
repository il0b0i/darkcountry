﻿using UnityEngine;

public class ShootingController_ : MonoBehaviour
{
    //public GameObject leftGun;
    //public GameObject rightGun;
    public bool isShootingLeft = false;
    public bool isShootingRight = false;
    public bool isReloading = false;
    private GunController leftGunController;
    private GunController rightGunController;
    //private PlayerController player;
    private bool setHolster = false;

    private void unHolstered()
    {
        if (GameManager.instance.playerLeftGun != null) leftGunController = GameManager.instance.playerLeftGun.GetComponent<GunController>(); else leftGunController = null;
        if (GameManager.instance.playerRightGun != null) rightGunController = GameManager.instance.playerRightGun.GetComponent<GunController>(); else rightGunController = null;
        Animator animator = this.GetComponent<Animator>();
        if (animator.GetBool("isAiming"))
            setHolster = true;
        else
            setHolster = false;

        //Debug.Log("Holster is " + setHolster);
        if (leftGunController) leftGunController.unHolstered = setHolster;
        if (rightGunController) rightGunController.unHolstered = setHolster;
    }
}