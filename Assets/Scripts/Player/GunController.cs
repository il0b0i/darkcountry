﻿using UnityEngine;

public class GunController : MonoBehaviour
{
    //GUN STATS
    public float gunRange = 50f;
    public float gunDamage = 40f;
    public int gunBullets = 6;
    public int bulletsInChamber = 1;
    [SerializeField]
    public enum FireMode
    {
        LightNormal,
        HeavyNormal,
        ChargedNormal,
    };
    public FireMode currentFireMode;
    public float shootSpeed = 10f;
    public float reloadSpeed = 10f;
    public int maxReflections = 2;
    public float forceMultiplier = 10f;
    public bool setActive = false;
    public bool isLeft = false;
    public bool isRight = false;
    public bool autoSwap = false;
    private int nReflections = 0;
    //LineRender material and colors
    [ColorUsageAttribute(true, true)] public Color lineRenderColorBase;
    [ColorUsageAttribute(true, true)] public Color lineRenderColorTarget;
    public Material lineRenderMaterial;
    //Shooting, swaping, and reloading timers
    private float nextShot = 2f;
    private bool canShootGun = false;
    private bool isReloading = false;
    private bool isShooting = false;
    private float currReloadTime = 0f;
    private int maxBullets = 0;
    [HideInInspector]
    public bool unHolstered = false;
    private bool canSwap = false;
    private float swapCoolDown = 2f;
    private float reloadTimer = 10f;
    private float maxReloadTimer = 0f;
    //Layermask
    public LayerMask targetLayerMask;

    //RayCast
    Ray ray;
    RaycastHit hit;
    Vector3 nDirection;

    //Gun effects
    //public GameObject parentGun;
    public ParticleSystem muzzleFlash;
    public ParticleSystem bulletTrail;
    public AudioClip gunShotSound;
    public AudioClip reloadSound;

    //Components
    GameObject player;
    PlayerController playerController;
    GunController parentGunController;
    LineRenderer lineRender;
    AudioSource gunAudioSource;
    Renderer gunRender;
    Animator animator;
    CamController camController;
    public Transform origin;
    //TimeBullet timeBullet;

    //ObjectPooler objectPooler;

    void Start()
    {
        //Get player
        player = GameManager.instance.Player;
        //Get components
        //playerController = player.GetComponent<PlayerController>();
        //timeBullet = GetComponent <TimeBullet>();
        if (isLeft && GameManager.instance.playerRightGun != null) parentGunController = GameManager.instance.playerRightGun.GetComponent<GunController>();
        else
        if (isRight && GameManager.instance.playerLeftGun != null) parentGunController = GameManager.instance.playerLeftGun.GetComponent<GunController>();
        gunAudioSource = GetComponent<AudioSource>();
        gunRender = GetComponent<Renderer>();
        animator = player.GetComponent<Animator>();
        lineRender = GetComponent<LineRenderer>();
        camController = player.GetComponent<CamController>();
        //Set Values
        currReloadTime = reloadSpeed;
        maxBullets = gunBullets;
        lineRender.startWidth = 0.05f;
        lineRender.endWidth = 0.05f;
        lineRenderMaterial.SetColor("_Color", lineRenderColorBase);
        lineRenderMaterial.SetColor("_EmissionColor", lineRenderColorBase);
        maxReloadTimer = reloadTimer;
        //Activate gun
        if (setActive)
            ActivateGun();
    }

    void Update()
    {
        if ((Input.GetKey(KeyCode.Mouse1) || Input.GetButton("Fire1")) && setActive && !isReloading && !animator.GetBool("isResting") && !animator.GetBool("isHurt")) //&& !playerController.isDashing)
        {
            animator.SetBool("isAiming", true);
            reloadTimer = maxReloadTimer;
            //Play aim/unholster animation
            if (isLeft && setActive)
            {
                animator.SetBool("aimLeft", true);
                animator.SetBool("aimRight", false);
            }
            else if (isRight && setActive)
            {
                animator.SetBool("aimRight", true);
                animator.SetBool("aimLeft", false);
            }
            //If already unholstered
            if (unHolstered)
            {
                //Activate line render
                if (setActive)
                {
                    lineRender.enabled = true;
                    AimLine();
                }
                else lineRender.enabled = false;
                //Shoot active gun
                if (Input.GetButton("Fire1") && setActive && unHolstered && canShootGun && canSwap)
                {
                    Shoot();
                }
            }
        }
        else
        {
            if (setActive)
                animator.SetBool("isAiming", false);
            lineRender.enabled = false;
        }

        //Activate gun render
        if (animator.GetBool("isAiming"))
        {
            if (!gunRender.enabled)
                gunRender.enabled = true;
        }
        else
            gunRender.enabled = false;
        //Reset next shot
        if (isShooting)
            nextShot -= shootSpeed * Time.deltaTime;

        if (nextShot <= 0)
            _NextShot();
        //Disable gun if null in game manager
        if (isLeft && GameManager.instance.playerLeftGun == null)
        {
            if (setActive)
            {
                Swap();
            }
            parentGunController.parentGunController = null;
            gunRender.enabled = false;
        }
        else if (isRight && GameManager.instance.playerRightGun == null)
        {
            if (setActive)
            {
                Swap();
            }
            parentGunController.parentGunController = null;
            gunRender.enabled = false;
        }
        //Reload gun
        if (gunBullets <= 0 && Input.GetKeyDown(KeyCode.Tab))
        {
            if (!isReloading)
            {
                gunAudioSource.PlayOneShot(reloadSound);
                canShootGun = false;
                isReloading = true;
            }
        }

        //Reload timer
        if ((gunBullets < maxBullets && reloadTimer > 0 && !animator.GetBool("isAiming"))) // || (gunBullets < maxBullets && reloadTimer > 0 && !setActive))
        {
            reloadTimer -= 5 * Time.deltaTime;

            if (reloadTimer <= 0 && !isReloading)
            {
                gunAudioSource.PlayOneShot(reloadSound);
                isReloading = true;
            }
        }

        if (isReloading)
        {
            currReloadTime -= reloadSpeed * Time.deltaTime;
        }

        if (currReloadTime <= 0)
            Reload();

        //Swap cooldown
        if (!canSwap && swapCoolDown > 0 && setActive)
        {
            ////Debug.Log(transform.name + " started swap cooldown");
            swapCoolDown -= 10 * Time.deltaTime;
        }

        if (swapCoolDown <= 0)
        {
            ////Debug.Log(transform.name + " can swap");
            canSwap = true;
            swapCoolDown = 2f;
        }
        //Can swap
        if (Input.GetKeyDown(KeyCode.Tab) && setActive && canSwap && parentGunController != null && !parentGunController.isReloading)
        {
            //Debug.Log(transform.name + " swap activated");
            Swap();
            canSwap = false;
        }

    }

    void Shoot()
    {
        var cam = Camera.main;
        switch (currentFireMode)
        {
            case FireMode.LightNormal:
                if (isLeft)
                {
                    bulletsInChamber = 1;
                    animator.Play("shooting_C", 2, Time.deltaTime);
                }
                else if (isRight)
                {
                    bulletsInChamber = 1;
                    animator.Play("shooting_B", 2, Time.deltaTime);
                }
                break;
            case FireMode.HeavyNormal:
                if (isLeft)
                {
                    bulletsInChamber = 2;
                    animator.Play("h_shooting_C", 2, Time.deltaTime);
                }
                else if (isRight)
                {
                    bulletsInChamber = 2;
                    animator.Play("h_shooting_B", 2, Time.deltaTime);
                }
                break;
        }
        gunAudioSource.PlayOneShot(gunShotSound);
        //starting frame/total frames * desired starting frame
        isShooting = true;
        gunBullets -= bulletsInChamber;

        canShootGun = false;
        muzzleFlash.Play();
        //bulletTrail.Play();
        Ray ray = cam.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0f));
        Debug.DrawRay(ray.origin, cam.transform.forward * gunRange, Color.green, 5, false);
        //If first shot
        for (int i = 0; i <= maxReflections; i++)
        {
            if (i == 0)
            {
                if (Physics.Raycast(ray.origin, ray.direction, out hit, gunRange, targetLayerMask))
                {
                    nDirection = Vector3.Reflect(ray.direction, hit.normal);
                    NewStateBehaviour target = hit.transform.GetComponent<NewStateBehaviour>();
                    if (target == null)
                        ray = new Ray(hit.point, nDirection);
                    if (target != null)
                    {
                        //Debug.Log(hit.transform.name);
                        target.Dodge();
                        target.stats.TakeDamage(gunDamage);                    
                    }
                    if (hit.rigidbody != null)
                    {
                        hit.rigidbody.AddForce(-hit.normal * (gunDamage + forceMultiplier));
                    }
                    Debug.DrawRay(hit.point, hit.normal * 3, Color.magenta, 5, false);
                    Debug.DrawRay(ray.origin, ray.direction * gunRange, Color.red, 5, false);
                }
            }
            else
            {
                //If at least 1 reflections
                if (Physics.Raycast(ray.origin, ray.direction, out hit, gunRange, targetLayerMask))
                {
                    nDirection = Vector3.Reflect(ray.direction, hit.normal);
                    NewStateBehaviour target = hit.transform.GetComponent<NewStateBehaviour>();
                    if (target == null)
                        ray = new Ray(hit.point, nDirection);
                    if (target != null)
                    {
                        //Debug.Log(hit.transform.name);
                        target.Dodge();
                        target.stats.TakeDamage(gunDamage * i);
                    }
                    if (hit.rigidbody != null)
                    {
                        hit.rigidbody.AddForce(-hit.normal * (gunDamage + forceMultiplier) * i);
                    }
                    Debug.DrawRay(hit.point, hit.normal * 3, Color.magenta, 5, false);
                    Debug.DrawRay(ray.origin, ray.direction * gunRange, Color.blue, 5, false);
                }
            }
        }
    }

    void AimLine()
    {
        var cam = Camera.main;
        lineRenderMaterial.SetColor("_Color", lineRenderColorBase);
        lineRenderMaterial.SetColor("_EmissionColor", lineRenderColorBase);
        nReflections = maxReflections;
        //Left Aim Line
        Ray ray = cam.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0f));
        //Debug.DrawRay(ray.origin, cam.transform.forward * gunRange, Color.blue, 5, false);
        lineRender.positionCount = nReflections;
        lineRender.SetPosition(0, origin.transform.position);
        lineRender.SetPosition(1, cam.transform.forward);
        //If first shot
        for (int i = 0; i <= maxReflections; i++)
        {
            if (i == 0)
            {
                if (Physics.Raycast(ray.origin, ray.direction, out hit, gunRange, targetLayerMask))
                {
                    nDirection = Vector3.Reflect(ray.direction, hit.normal);
                    NewStateBehaviour target = hit.transform.GetComponent<NewStateBehaviour>();
                    lineRender.enabled = true;
                    lineRender.positionCount = ++nReflections;
                    lineRender.SetPosition(i + 1, hit.point);
                    if (target == null)
                    {
                        ray = new Ray(hit.point, nDirection);
                        lineRender.SetPosition(nReflections - 1, ray.GetPoint(gunRange));
                    }
                    else if (target != null)
                    {
                        camController.target = hit.transform;
                        target.Dodge();
                        lineRender.SetPosition(nReflections - 1, hit.point);
                    }
                        
                    //CharacterStats target = hit.transform.GetComponent<CharacterStats>();
                    //if (target != null) 
                    //{
                    //	//Debug.Log("SHOOT!");
                    //}
                }
                else
                {
                    lineRender.enabled = false;
                }
            }
            //If at least 1 reflections
            else
            {
                if (Physics.Raycast(ray.origin, ray.direction, out hit, gunRange, targetLayerMask))
                {
                    nDirection = Vector3.Reflect(ray.direction, hit.normal);
                    NewStateBehaviour target = hit.transform.GetComponent<NewStateBehaviour>();
                    lineRender.positionCount = ++nReflections;
                    lineRender.SetPosition(i + 1, hit.point);
                    if (target == null)
                    {
                        ray = new Ray(hit.point, nDirection);
                        lineRender.SetPosition(nReflections - 1, ray.GetPoint(gunRange));
                    }

                    if (target != null)
                    {
                        camController.target = hit.transform;
                        target.Dodge();
                        //if (!timeBullet.isSlow)
                        //    timeBullet.BulletTime();
                        lineRenderMaterial.SetColor("_Color", lineRenderColorTarget);
                        lineRenderMaterial.SetColor("_EmissionColor", lineRenderColorTarget);
                        ////Debug.Log("SHOOT LEFT!");
                        lineRender.SetPosition(nReflections - 1, hit.point);
                    }
                }
            }
        }
    }

    void _NextShot()
    {
        if (isShooting && gunBullets > 0)
        {
            canShootGun = true;
            nextShot = 2f;
            isShooting = false;
        }
        else if (isShooting && gunBullets <= 0)
        {
            if (autoSwap)
                Swap();
            nextShot = 2f;
            isShooting = false;
        }
        else if (isShooting && gunBullets > 0)
        {
            canShootGun = true;
            nextShot = 2f;
            isShooting = false;
        }
        else if (isShooting && gunBullets <= 0)
        {
            if (autoSwap)
                Swap();
            nextShot = 2f;
            isShooting = false;
        }
    }

    void Reload()
    {
        reloadTimer = maxReloadTimer;
        isReloading = false;
        //shootingController.isReloading = false;
	    currReloadTime = reloadSpeed;
	    gunBullets = maxBullets;
        if (setActive)
            canShootGun = true;
    }

    void Swap()
    {
        //Debug.Log(transform.name + " Swap");
        animator.ResetTrigger("swapRight");
        animator.ResetTrigger("swapLeft");

        if (isLeft && setActive)
        {
            //Debug.Log(transform.name + " swap to right");
            if (animator.GetBool("aimLeft"))
                animator.SetTrigger("swapRight");
            parentGunController.ActivateGun();
            //Debug.Log(transform.name + " deactivated");
            setActive = false;
        }
        else if (isRight && setActive)
        {
            //Debug.Log(transform.name + " swap to left");
            if (animator.GetBool("aimRight"))
                animator.SetTrigger("swapLeft");
            parentGunController.ActivateGun();
            //Debug.Log(transform.name + " deactivated");
            setActive = false;
        }
    }

    void ActivateGun()
    {
        //Debug.Log(transform.name + " activated");
        setActive = true;
        if (setActive && gunBullets > 0)
        {
            canShootGun = true;
        }
    }
}