﻿using UnityEngine;

public class PlayerController : MonoBehaviour
{
    /* Player health variables */
    // Current health (range 3-?)
    public int health = 3;
    // Max possible health (range 3-?)
    public int maxHealth = 3;
    // Current regeneration slots (range 3-?)
    public int regenSlots = 3;
    // Max possible regeneration slots (range 3-?)
    public int maxRegenSlots = 3;

    /* Player stamina variables */
    // Current stamina (range 3-?)
    public float stamina = 3.0f;
    // Maximum stamina (range 3-?)
    public float maxStamina = 3.0f;
    // The rate at which stamina regenerates
    public float staminaRegenRate = 0.025f;
    // The current stamina regeneration cooldown
    public float staminaRegenCooldown = 0f;
    // Stamina regeneration cooldown limit
    public float staminaRegenCooldownLimit = 4.0f;
    // The rate at which the stamina regeneration coolsdown
    public float staminaRegenCooldownRate = 0.05f;

    /* Player movement variables */
    // Player speed
    [Range(0f, 15f)]
    public float maxSpeed = 0f;
    public float speed = 150f;
    float playerSpeed;
    // Player speed when walking backwards
    public float backwardsSpeed = 100f;
    // Mutliplier of the speed when running
    public float runMultiplier = 2f;
    // The rate at which stamina drains while running
    public float runStaminaDrainRate = 0.0025f;
    //Blend tree parameters
    public float vely = 0f;
    public float velx = 0f;

    //Player states
    bool inMotion = false;
    bool isRunning = false;

    //How far the raycast checks to check if grounded
    public float GroundedLimit = 0.5f;

    /* Player state variables */
    public bool isGrounded = false;
    public bool canMove = true;
    public bool isUsingStamina = false;
    public bool aimFocused = false;
    private const float ROTATION_SPEED = 6f;
    public PlayerStats stats;
    DashController dashController;
    [HideInInspector]
    public Animator animator;
    Rigidbody rigidBody;
    public Camera floatingCamera;
    [HideInInspector]
    public PlayerAudioManager audioManager;

    void Start()
    {
        rigidBody = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();
        dashController = GetComponent<DashController>();
        stats = GetComponent<PlayerStats>();
        floatingCamera = Camera.main;
        audioManager = GetComponent<PlayerAudioManager>();
    }

    void FixedUpdate()
    {
        float playerSpeed = speed;
        Vector3 rigidBodyForward = rigidBody.transform.forward;
        Quaternion rigidBodyRotation = rigidBody.transform.rotation;
        Vector3 cameraForward = floatingCamera.transform.forward;
        Quaternion cameraRotation = floatingCamera.transform.rotation;
        Vector3 forward = Vector3.Normalize(new Vector3(cameraForward.x, 0, cameraForward.z));
        Vector3 right = Vector3.Normalize(new Vector3(floatingCamera.transform.right.x, 0, floatingCamera.transform.right.z));

        // Handle forward motion movement
        if (Input.GetKey(KeyCode.W) && dashController.dashCurrentCooldown <= 0.0f && canMove)
        {
            if (rigidBody.velocity.sqrMagnitude < maxSpeed)
                rigidBody.AddForce(forward * playerSpeed, ForceMode.Acceleration);

            vely = Mathf.Lerp(vely, maxSpeed, 5 * Time.deltaTime);

            rigidBody.rotation = Quaternion.Slerp(transform.rotation, RotateY(cameraRotation.eulerAngles.y), ROTATION_SPEED * Time.deltaTime);
        }
        else if (Input.GetKey(KeyCode.S) && dashController.dashCurrentCooldown <= 0.0f && canMove)
        {
            if (rigidBody.velocity.sqrMagnitude < maxSpeed)
                rigidBody.AddForce(-forward * playerSpeed, ForceMode.Acceleration);

            if (animator.GetBool("isAiming"))
                vely = Mathf.Lerp(vely, -0.5f, 5 * Time.deltaTime);
            else
                vely = Mathf.Lerp(vely, maxSpeed, 5 * Time.deltaTime);

            rigidBody.rotation = Quaternion.Slerp(transform.rotation, RotateY(cameraRotation.eulerAngles.y - 180), ROTATION_SPEED * Time.deltaTime);
        }

        // Handle rightward motion movement
        if (Input.GetKey(KeyCode.A) && dashController.dashCurrentCooldown <= 0.0f && canMove)
        {
            if (rigidBody.velocity.sqrMagnitude < maxSpeed)
                rigidBody.AddForce(-right * playerSpeed, ForceMode.Acceleration);

            if (animator.GetBool("isAiming"))
            {
                vely = Mathf.Lerp(vely, 0f, 5 * Time.deltaTime);
                velx = Mathf.Lerp(velx, -7f, 5 * Time.deltaTime);
            }
            else
            {
                vely = Mathf.Lerp(vely, maxSpeed, 5 * Time.deltaTime);
                velx = Mathf.Lerp(velx, 0f, 5 * Time.deltaTime);
            }

            if (Input.GetKey(KeyCode.W))
            {
                //rigidBody.velocity = (-right + forward) * playerSpeed;
                rigidBody.rotation = Quaternion.Slerp(transform.rotation, RotateY(cameraRotation.eulerAngles.y - 45), ROTATION_SPEED * Time.deltaTime);
            }
            else if (Input.GetKey(KeyCode.S))
            {
                //rigidBody.velocity = (-right + -forward) * playerSpeed;
                rigidBody.rotation = Quaternion.Slerp(transform.rotation, RotateY(cameraRotation.eulerAngles.y + 225), ROTATION_SPEED * Time.deltaTime);
            }
            else
                rigidBody.rotation = Quaternion.Slerp(transform.rotation, RotateY(cameraRotation.eulerAngles.y + 270), ROTATION_SPEED * Time.deltaTime);
        }
        else if (Input.GetKey(KeyCode.D) && dashController.dashCurrentCooldown <= 0.0f && canMove)
        {
            if (rigidBody.velocity.sqrMagnitude < maxSpeed)
                rigidBody.AddForce(right * playerSpeed, ForceMode.Acceleration);

            if (animator.GetBool("isAiming"))
            {
                vely = Mathf.Lerp(vely, 0f, 5 * Time.deltaTime);
                velx = Mathf.Lerp(velx, 7f, 5 * Time.deltaTime);
            }
            else
            {
                vely = Mathf.Lerp(vely, maxSpeed, 5 * Time.deltaTime);
                velx = Mathf.Lerp(velx, 0f, 5 * Time.deltaTime);
            }

            if (Input.GetKey(KeyCode.W))
            {
                //rigidBody.velocity = (right + forward) * playerSpeed;
                rigidBody.rotation = Quaternion.Slerp(transform.rotation, RotateY(cameraRotation.eulerAngles.y + 45), ROTATION_SPEED * Time.deltaTime);
            }
            else if (Input.GetKey(KeyCode.S))
            {
                //rigidBody.velocity = (right + -forward) * playerSpeed;
                rigidBody.rotation = Quaternion.Slerp(transform.rotation, RotateY(cameraRotation.eulerAngles.y - 225), ROTATION_SPEED * Time.deltaTime);
            }
            else
                rigidBody.rotation = Quaternion.Slerp(transform.rotation, RotateY(cameraRotation.eulerAngles.y + 90), ROTATION_SPEED * Time.deltaTime);
        }
        else
            velx = Mathf.Lerp(velx, 0f, 5 * Time.deltaTime);

        ShootingController_ shootController = this.GetComponent<ShootingController_>();
        // TODO: Make it an instance var? 🤔
        bool canAim =
             !shootController.isReloading
          && !animator.GetBool("isHurt")
          && !animator.GetBool("isResting") && isGrounded;

        if (Input.GetButton("Fire1") && canAim)
        {
            animator.SetBool("isAiming", true);
            rigidBody.rotation = Quaternion.Slerp(transform.rotation, RotateY(floatingCamera.transform.rotation.eulerAngles.y), ROTATION_SPEED * Time.deltaTime);

            if (shootController.isShootingLeft || shootController.isShootingRight)
                animator.SetBool("isShooting", true);
            else
                animator.SetBool("isShooting", false);
        }
        else if (Input.GetKey(KeyCode.Mouse1) && canAim)
        {
            aimFocused = true;
            animator.SetBool("isAiming", true);
            rigidBody.rotation = Quaternion.Slerp(transform.rotation, RotateY(floatingCamera.transform.rotation.eulerAngles.y), ROTATION_SPEED * Time.deltaTime);

            if (shootController.isShootingLeft || shootController.isShootingRight)
                animator.SetBool("isShooting", true);
            else
                animator.SetBool("isShooting", false);
        }

        else
        {
            aimFocused = false;
            animator.SetBool("isAiming", false);
            animator.SetBool("isShooting", false);
        }

    }
    void Update()
    {
        //If velocity is not 0 then animate
        if ((Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.D)) && canMove)
        {
            inMotion = true;
            if (Input.GetKey(KeyCode.LeftShift) && !animator.GetBool("isAiming"))
            {
                if (stamina > 0.0f)
                {
                    maxSpeed = Mathf.Lerp(maxSpeed, 15f, 2 * Time.deltaTime);
                    stamina -= runStaminaDrainRate;
                    isUsingStamina = true;
                    playerSpeed *= runMultiplier;
                    isRunning = true;
                }
                else
                {
                        maxSpeed = Mathf.Lerp(maxSpeed, 0.5f, 2 * Time.deltaTime);
                }
            }
            else
            {
                maxSpeed = Mathf.Lerp(maxSpeed, 0.5f, 2 * Time.deltaTime);
                isUsingStamina = false;
            }
        }
        else
            inMotion = false;

        //Check if moving
        if (!inMotion)
        {
            maxSpeed = Mathf.Lerp(maxSpeed, 0f, 2 * Time.deltaTime);
            vely = Mathf.Lerp(vely, 0f, 2 * Time.deltaTime);
            velx = Mathf.Lerp(velx, 0f, 2 * Time.deltaTime);
            isUsingStamina = false;
        }

        //Stamina propiertieesss
        if (isUsingStamina)
        {
            // Reset the regen cooldown if using stamina
            if (stamina < maxStamina)
                staminaRegenCooldown = 0f;
        }
        else
        {
            // If not using stamina and not at limit
            if (stamina < maxStamina && staminaRegenCooldown < staminaRegenCooldownLimit)
                staminaRegenCooldown += staminaRegenCooldownRate;
            else if (stamina < maxStamina && staminaRegenCooldown >= staminaRegenCooldownLimit)
                stamina += staminaRegenRate;
            else if (stamina >= maxStamina && stamina >= staminaRegenCooldownLimit)
                staminaRegenCooldown = 0f;
            stamina = Mathf.Clamp(stamina, 0f, maxStamina);
        }

        //Update animator inMotion bool
        animator.SetBool("inMotion", inMotion);
        //Update animator speed (vely) parameter
        animator.SetFloat("vely", vely);
        //Update animator direction (velx) parameter
        animator.SetFloat("velx", velx);

        //Raycast checks if floor is below
        RaycastHit hit;
        Ray ray = new Ray((transform.position + transform.forward * 0.1f) + transform.up * 0.5f, -transform.up);
        Debug.DrawRay(ray.origin, ray.direction * GroundedLimit, Color.blue);
        if (Physics.Raycast(ray, out hit, GroundedLimit))
        {
            if (hit.transform.name == "Floor")
                isGrounded = true;
        }
        else
            isGrounded = false;


        //Grounded
        if (!isGrounded)
        {
            if (animator.GetBool("isGrounded") && rigidBody.velocity.sqrMagnitude < 50f)
            {
                rigidBody.drag = 0f;
                animator.CrossFade("Fall_A", 0.25f);
                animator.SetBool("isGrounded", isGrounded);
            }
        }
        else
        {
            if (!animator.GetBool("isGrounded"))
            {
                animator.CrossFade("Fall_land", Time.deltaTime);
                animator.SetBool("isGrounded", isGrounded);
            }
        }


        //Resting//
        if (Input.GetKeyDown(KeyCode.K))
        {
            if (!animator.GetBool("isResting"))
            {
                canMove = false;
                animator.SetBool("isResting", true);
            }
            else
            {
                canMove = true;
                animator.SetBool("isResting", false);
            }
        }
    }
    void LateUpdate()
    {
        //GameObject playerTorso = GameObject.FindGameObjectWithTag("PlayerTorso");
        GameObject playerTorso = GameManager.instance.PlayerTorsoBone;

        if (animator.GetBool("isAiming"))
        {
            // playerTorso.transform.Rotate(new Vector3(0, floatingCamera.transform.rotation.eulerAngles.x, 0));
            playerTorso.transform.Rotate(new Vector3(0, floatingCamera.transform.rotation.eulerAngles.x, 0));
        }
    }

    //void OnCollisionEnter(Collision collision)
    //{
    // if (collision.gameObject.tag == "PushableObject")
    // isPushingObject = true;
    //}

    //void OnCollisionExit(Collision collision)
    //{
    // if (collision.gameObject.tag == "PushableObject")
    // isPushingObject = false;
    //}

    private Quaternion RotateY(float degrees)
    {
        float xRotation = floatingCamera.transform.rotation.x;
        float zRotation = floatingCamera.transform.rotation.z;

        Quaternion correctedCameraRotation = Quaternion.Euler(xRotation, 0, zRotation);
        return correctedCameraRotation * Quaternion.Euler(0, degrees, 0);
    }

    //If hurt
    private void hurtStart()
    {
        stats.invFrames = true;
        canMove = false;
        animator.SetBool("isDashing", false);
        animator.SetBool("isResting", false);
    }
    private void hurtEnd()
    {
        stats.invFrames = false;
        canMove = true;
        animator.SetBool("isHurt", false);
    }

    //Handle fall landing

    private void disableMovement()
    {
        canMove = false;
    }

    private void enableMovement()
    {
        canMove = true;
    }
}
