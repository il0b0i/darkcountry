﻿using UnityEngine;

public class FloatingCameraController : MonoBehaviour
{
  public Transform lookAtDefault;
  public Transform lookAtAiming;
  public Transform camTransform;
  // The lower the faster
  public float cameraSmoothness = 0.1f;
  public float camMaxDistance = 15f;

  [HideInInspector]
  public float distance = 15f;
  public float sensitivity = 6f;
  public float minAngleClamp = -30f;
  public float minAngleClampAiming = -30f;
  public float maxAngleClamp = 20f;
  public float maxAngleClampAiming = 60f;
  public LayerMask camOcclusion;
  private PlayerController player;
  private Camera cam;
  private Vector3 cameraVelocity = Vector3.zero;
  private float currentX = 0f;
  private float currentY = 0f;
  Animator animator;

    

  void Start()
  {
    distance = camMaxDistance;
    camTransform = transform;
    player = GameManager.instance.Player.GetComponent<PlayerController>();
    animator = player.GetComponent<Animator>();
    cam = Camera.main;
    Cursor.lockState = CursorLockMode.Locked;
    Cursor.visible = false;
  }

  void Update()
  {
    currentX += Input.GetAxis("Mouse X") * sensitivity;
    currentY += Input.GetAxis("Mouse Y") * sensitivity;

    if (animator.GetBool("isAiming"))
      currentY = Mathf.Clamp(currentY, minAngleClampAiming, maxAngleClampAiming);
    else
      currentY = Mathf.Clamp(currentY, minAngleClamp, maxAngleClamp);
  }

  void LateUpdate()
  {
    Transform lookAt = lookAtDefault;
    Vector3 dir = new Vector3(0, 0, -distance);
    Quaternion rotation = Quaternion.Euler(-currentY, currentX, 0);
    RaycastHit wallHit = new RaycastHit();

    if (animator.GetBool("isAiming") && player.aimFocused)
    {
      distance = camMaxDistance / 3;
      lookAt = lookAtAiming;
    }
    else
    {
      distance = camMaxDistance;
      lookAt = lookAtDefault;
    }

    // camTransform.position = Vector3.Lerp(camTransform.position, lookAt.position + rotation * dir, 500f * Time.deltaTime);
    Vector3 l = lookAt.position + rotation * dir;

    // camTransform.position = Vector3.SmoothDamp(camTransform.position, l, ref cameraVelocity, cameraSmoothness);
    // camTransform.position = Vector3.Lerp(camTransform.position, lookAt.position + rotation * dir, 500f * Time.deltaTime);
    camTransform.position = l;
    camTransform.LookAt(lookAt.position);

    if (Physics.Linecast(lookAt.position, camTransform.position, out wallHit, camOcclusion))
    {
      if (!(wallHit.rigidbody != null && wallHit.rigidbody.tag == "Player"))
        camTransform.position = new Vector3(wallHit.point.x + wallHit.normal.x * 0.5f, camTransform.position.y, wallHit.point.z + wallHit.normal.z * 0.5f);
    }
  }
}
