﻿using UnityEngine;

public class TimeBullet : MonoBehaviour {

    public float timeBulletFactor = 0.05f;
    public float timeBulletLenght = 1f;

    void Update()
    {
        Time.timeScale += (1 / timeBulletLenght) * Time.unscaledDeltaTime;
        Time.timeScale = Mathf.Clamp(Time.timeScale, 0f, 1f);
    }
    public void BulletTime()
    {
        Time.timeScale = timeBulletFactor;
        Time.fixedDeltaTime = Time.timeScale * 0.02f;
    }
}
