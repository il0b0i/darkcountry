﻿using UnityEngine;

public class DashController : MonoBehaviour
{
    /* Dash variables */
    public Quaternion dashRotation;
    public string dashDirection;
    public float dashThrust = 100f;
    public float dashCurrentTime = 1f;
    public float dashMaxTime = 1f;
    public float dashIncrement = 0.1f;
    public float dashCurrentCooldown = 0f;
    public float dashCooldownDecrement = 0.1f;
    public bool isDashingDirectionally;
    float thrust;

    //References
    public LayerMask obstacleMask;
    public LayerMask npcMasK;
    PlayerController player;
    Rigidbody rigidBody;
    Animator animator;


    void Start()
    {
        player = GameManager.instance.Player.GetComponent<PlayerController>();

        rigidBody = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();
    }

    void FixedUpdate()
    {
        if (Input.GetKeyDown(KeyCode.Space) && player.isGrounded && player.canMove && dashCurrentCooldown <= 2.0f && player.stamina >= 1f)
        {
            //if (player.stamina >= 0.5f && player.stamina <= 1.0f)
            //    thrust = dashThrust * player.stamina;
            //else
                thrust = dashThrust;

            if (Input.GetKey(KeyCode.Mouse1) || Input.GetButton("Fire1"))
            {
                if (Input.GetKey(KeyCode.W))
                    animator.Play("DashFront", 0, Time.deltaTime);
                else if (Input.GetKey(KeyCode.S))
                    animator.Play("DashBack", 0, Time.deltaTime);

                if (
                  Input.GetKey(KeyCode.A)
                  || (Input.GetKey(KeyCode.A) && Input.GetKey(KeyCode.W))
                  || (Input.GetKey(KeyCode.A) && Input.GetKey(KeyCode.S))
                )
                    animator.Play("DashLeft", 0, Time.deltaTime);
                else if (
                  Input.GetKey(KeyCode.D)
                  || (Input.GetKey(KeyCode.D) && Input.GetKey(KeyCode.W))
                  || (Input.GetKey(KeyCode.D) && Input.GetKey(KeyCode.S))
                )
                    animator.Play("DashRight", 0, Time.deltaTime);
            }
            else
                animator.Play("DashFront", 0, Time.deltaTime);

            if (Input.GetKey(KeyCode.W))
            {
                if (Input.GetKey(KeyCode.A))
                    dashDirection = "Northwest";
                else if (Input.GetKey(KeyCode.D))
                    dashDirection = "Northeast";
                else
                    dashDirection = "North";
            }
            else if (Input.GetKey(KeyCode.S))
            {
                if (Input.GetKey(KeyCode.A))
                    dashDirection = "Southwest";
                else if (Input.GetKey(KeyCode.D))
                    dashDirection = "Southeast";
                else
                    dashDirection = "South";
            }
            else if (Input.GetKey(KeyCode.A))
                dashDirection = "West";
            else if (Input.GetKey(KeyCode.D))
                dashDirection = "East";
            else if (!Input.GetKey(KeyCode.W) && !Input.GetKey(KeyCode.A) && !Input.GetKey(KeyCode.S) && !Input.GetKey(KeyCode.D))
            {
                dashDirection = "South";
                animator.Play("DashBack", 0, Time.deltaTime);
            }

            dashCurrentTime = 0f;
            dashCurrentCooldown = 4f;
        }

        if (dashCurrentTime < dashMaxTime && animator.GetBool("isDashing"))
        {
            Vector3 direction = rigidBody.transform.forward;
            // Vector3 force = new Vector3(rigidBody.transform.forward.x, rigidBody.transform.forward.y, rigidBody.transform.forward.z);
            // rigidBody.AddForce(force * dashThrust, ForceMode.Acceleration);

            player.isUsingStamina = true;

            if (isDashingDirectionally)
            {
                switch (dashDirection)
                {
                    case "North":
                        direction = rigidBody.transform.forward;
                        break;
                    case "Northwest":
                        direction = rigidBody.transform.forward + (-rigidBody.transform.right);
                        break;
                    case "Northeast":
                        direction = rigidBody.transform.forward + rigidBody.transform.right;
                        break;
                    case "South":
                        direction = -rigidBody.transform.forward;
                        break;
                    case "Southwest":
                        direction = -rigidBody.transform.forward + (-rigidBody.transform.right);
                        break;
                    case "Southeast":
                        direction = -rigidBody.transform.forward + rigidBody.transform.right;
                        break;
                    case "West":
                        direction = -rigidBody.transform.right;
                        break;
                    case "East":
                        direction = rigidBody.transform.right;
                        break;
                }
                // rigidBody.velocity = rigidBody.
            }
            else
            {
                direction = rigidBody.transform.forward;
            }

            rigidBody.drag = 10f;
            rigidBody.rotation = dashRotation;
            rigidBody.AddForce(direction * thrust, ForceMode.Impulse);
            dashCurrentTime += dashIncrement;
        }
        else
        {
            if (dashCurrentCooldown > 0f)
                dashCurrentCooldown -= dashCooldownDecrement;

            // rigidBody.velocity = Vector3.zero;
            animator.SetBool("isDashing", false);
            isDashingDirectionally = false;
        }

    }

    void Update()
    {
        //Check collision when dashing
        if (animator.GetBool("isDashing") && !isDashingDirectionally)
        {
            RaycastHit hit;
            Ray ray = new Ray(transform.position + Vector3.up, transform.forward);
            Debug.DrawRay(ray.origin, transform.forward * 2f, Color.blue);

            if (Physics.Raycast(ray.origin, ray.direction, out hit, 1f, obstacleMask))
            {
                    Debug.Log("HITS: " + hit.transform.name + hit.transform.tag);
                    if (!animator.GetBool("isHurt"))
                        animator.SetBool("isHurt", true);
                    animator.CrossFade("Hurt_A", 0.25f);
            }
            else if (Physics.Raycast(ray.origin, ray.direction, out hit, 2f, npcMasK))
            {
                CharacterStats target = hit.transform.GetComponent<CharacterStats>();
                if (target != null)
                {
                    if (!target.controllerAI.animator.GetBool("isHurt"))
                        target.controllerAI.animator.SetBool("isHurt", true);
                    target.controllerAI.animator.CrossFade("Hurt_A", 0.25f);
                    //target.SetKinematic(false);
                    hit.rigidbody.AddForce(-hit.normal * (20f));
                }
            }
        }
    }

    private void DashFrontBegin()
    {
        player.canMove = false;
        // Debug.Log("HEy");
        // rigidBody.AddForce(rigidBody.transform.forward * 500f, ForceMode.Impulse);
        player.stats.invFrames = true;
        Quaternion rotation = new Quaternion();
        float camRotationY = player.floatingCamera.transform.rotation.eulerAngles.y;

        player.stamina -= 1;
        player.isUsingStamina = true;

        switch (dashDirection)
        {
        case "North":
            rotation = Quaternion.Euler(0, camRotationY + 0, 0);
            break;
         case "Northwest":
            rotation = Quaternion.Euler(0, camRotationY + 315, 0);
            break;
        case "Northeast":
            rotation = Quaternion.Euler(0, camRotationY + 45, 0);
            break;
        case "South":
            rotation = Quaternion.Euler(0, camRotationY + 180, 0);
            break;
        case "Southwest":
            rotation = Quaternion.Euler(0, camRotationY + 225, 0);
            break;
        case "Southeast":
            rotation = Quaternion.Euler(0, camRotationY + 135, 0);
            break;
        case "West":
            rotation = Quaternion.Euler(0, camRotationY + 270, 0);
            break;
        case "East":
            rotation = Quaternion.Euler(0, camRotationY + 90, 0);
            break;
        default:
            rotation = Quaternion.Euler(0, camRotationY + 0, 0);
            break;
        }

        dashRotation = rotation;
        animator.SetBool("isDashing", true);
    }

    private void invDashFrameEnd()
    {
        player.stats.invFrames = false;
    }

    private void dashEnd()
    {
        player.canMove = true;
        rigidBody.drag = 0f;
    }

    private void DashDirectionalBegin()
    {
        player.canMove = false;
        player.stamina -= 1;

        dashRotation = Quaternion.Euler(0, player.floatingCamera.transform.rotation.eulerAngles.y, 0);
        animator.SetBool("isDashing", true);
        isDashingDirectionally = true;
    }
}
