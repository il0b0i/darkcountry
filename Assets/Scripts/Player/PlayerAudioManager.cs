﻿using UnityEngine;


public class PlayerAudioManager : MonoBehaviour
{
    public string groundType;
    public int currGround = 0;
    public int footStepNum = 0;
    //
    [SerializeField]
    private AudioClip[] unHolsterAudio;
    [SerializeField]
    private AudioClip[] clothAudio;
    [SerializeField]
    private AudioClip[] stoneFootSteps;
    [SerializeField]
    private AudioClip[] dirtFootSteps;
    [SerializeField]
    private AudioClip[] woodFootSteps;
    [SerializeField]
    private AudioClip[] waterFootSteps;
    [SerializeField]
    private AudioClip[] hurtAudio;
    //
    private AudioSource audioSource;

    void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    //Foot steps
    void Update()
    {
        RaycastHit hit;
        Ray ray = new Ray(transform.position + transform.up, -transform.up);
        if (Physics.Raycast(ray, out hit, 1.1f))
        {
            if (hit.transform.tag == "DirtGrassGround")
            {
                groundType = "Dirt Ground";
                currGround = 1;
            }
            else if (hit.transform.tag == "StoneGround")
            {
                groundType = "Stone Ground";
                currGround = 2;
            }
            else if (hit.transform.tag == "WoodGround")
            {
                groundType = "Wood Ground";
                currGround = 3;
            }
            else if (hit.transform.tag == "Water")
            {
                groundType = "Water";
                currGround = 4;
            }
        }
    }

    void OnCollisionExit(Collision hit)
    {
        //groundType = "Null";
        //currGround = 0;		
    }

    private void footStep()
    {
        if (footStepNum == 0)
            footStepNum = 1;
        else footStepNum = 0;
        if (currGround == 1)
        {
            AudioClip clip = GetDirtSteps();
            audioSource.PlayOneShot(clip);
        }
        else if (currGround == 2)
        {
            AudioClip clip = GetStoneSteps();
            audioSource.PlayOneShot(clip);
        }
        else if (currGround == 3)
        {
            AudioClip clip = GetWoodSteps();
            audioSource.PlayOneShot(clip);
        }
        else if (currGround == 4)
        {
            AudioClip clip = GetWaterSteps();
            audioSource.PlayOneShot(clip);
        }
        //else if (currGround == 0)
        //{
           // audioSource.Stop();
        //}
    }

    private AudioClip GetDirtSteps()
    {
        return dirtFootSteps[footStepNum];
    }

    private AudioClip GetStoneSteps()
    {
        return stoneFootSteps[footStepNum];
    }

    private AudioClip GetWoodSteps()
    {
        return woodFootSteps[footStepNum];
    }

    private AudioClip GetWaterSteps()
    {
        return waterFootSteps[footStepNum];
    }

    //Unholster
    private void unHolstered()
    {
        AudioClip clip = GetUnHolster_1();
        audioSource.PlayOneShot(clip);
    }
    private AudioClip GetUnHolster_1()
    {
        return unHolsterAudio[0];
    }
	//Colth A
	private void clothPlayAudio_A()
	{	
		AudioClip clip = GetClothAudio_A();
		audioSource.PlayOneShot(clip);
    }
	private AudioClip GetClothAudio_A()
	{
		return clothAudio[0];
	}
	//Colth B
	private void clothPlayAudio_B()
	{	
		AudioClip clip = GetClothAudio_B();
		audioSource.PlayOneShot(clip);
    }
	private AudioClip GetClothAudio_B()
	{
		return clothAudio[Random.Range(1, clothAudio.Length)];
	}
    //Hurt
    private void hurtStart()
    {

        AudioClip clip = GetHurtAudio_WallHit();
        audioSource.PlayOneShot(clip);
        //audioSource.PlayOneShot(clip);
    }
    private AudioClip GetHurtAudio_WallHit()
    {
        return hurtAudio[Random.Range(0, hurtAudio.Length)];
    }
}