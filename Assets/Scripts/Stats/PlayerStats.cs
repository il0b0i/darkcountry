﻿using UnityEngine;

public class PlayerStats : MonoBehaviour
{
    public int health = 3;
    public float resistance = 100f;
    public float resistanceMultiplier = 100f;
    public bool invFrames;
    [HideInInspector]
    public Animator animator;

    void Start()
    {
        animator = this.GetComponent<Animator>();
    }

    public void TakeDamage(int amount)
    {
        if (!invFrames)
        {
            health -= amount;
            resistance -= amount * resistanceMultiplier;
        }
        if (resistance <= 0f)
        {
            if (!animator.GetBool("isHurt"))
                animator.SetBool("isHurt", true);
            resistance = 100f;
        }
        if (health <= 0f)
        {
            Die();
        }
    }

    public void Die()
    {
        health = 3;
    }
}
