﻿using UnityEngine;

public class CharacterStats : MonoBehaviour 
{
	public float health = 100f;
    public float resistance = 100f;
    public int damage = 1;

    [HideInInspector]
    public NewStateBehaviour controllerAI;
    [HideInInspector]
    public Animator animator;

    public void SetKinematic(bool newValue)
    {
        Rigidbody[] rb = GetComponentsInChildren<Rigidbody>() as Rigidbody[];
        foreach (Rigidbody body in rb)
        {
            body.isKinematic = newValue;
        }
    }

    void Start()
    {
        SetKinematic(true);
        controllerAI = GetComponent<NewStateBehaviour>();
        animator = GetComponent<Animator>();
    }

    public void TakeDamage (float amount)
	{
		health -= amount;
        resistance -= amount * 2;
        controllerAI.IsAlerted = true;
        if (resistance <= 0f)
        {
            if (!animator.GetBool("isHurt"))
                animator.SetBool("isHurt", true);
            animator.CrossFade("Hurt_A", 0.25f);
            resistance = 100f;
        }
        if (health <= 0f)
		{
			Die();
		}
	}

	public void Die()
	{
        controllerAI.IsAlive = false;
        controllerAI.agent.isStopped = true;
        SetKinematic(false);
        GetComponent<CapsuleCollider>().enabled = false;
        GetComponent<Animator>().enabled = false;
        //GetComponent<Animator>().enabled = false;
        //Destroy(gameObject, 10f);
	}
}
