﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlockAI : MonoBehaviour {

	public float speed = 0.5f;
	float rotationSpeed = 4.0f;
	Vector3 averageHeading;
	Vector3 averagePostion;
	float neighbourDistance = 2.0f;

	bool turning = false;

	// Use this for initialization
	void Start () {
		speed = Random.Range(0.5f, 1f);
	}
	
	// Update is called once per frame
	void Update () {

		if (Vector3.Distance(transform.position, FishFlock.goalPos) >= FishFlock.tankSize)
		{
			turning = true;
		}
		else
		{
			turning = false;
		}
		if (turning)
		{
			Vector3 direction = FishFlock.goalPos - transform.position;
			transform.rotation = Quaternion.Slerp(transform.rotation,
												Quaternion.LookRotation(direction),
												rotationSpeed * Time.deltaTime);
			speed = Random.Range(0.5f, 1f);
		}
		else
		{
			if (Random.Range(0,5) < 1)
				ApplyRules();

		}
		transform.Translate(0, 0, Time.deltaTime * speed);
	}

	void ApplyRules()
	{
		GameObject[] gos;
		gos = FishFlock.allFish;

		Vector3 vcentre = FishFlock.goalPos;
		Vector3 vavoid = FishFlock.goalPos;
		float gSpeed = 0.1f;

		Vector3 goalPos = FishFlock.goalPos;

		float dist;

		int groupSize = 0;
		foreach (GameObject go in gos)
		{
			if (go != this.gameObject)
			{
				dist = Vector3.Distance(go.transform.position,this.transform.position);
				if (dist <= neighbourDistance)
				{
					vcentre += go.transform.position;
					groupSize++;

					if (dist < 1.0f)
					{
						vavoid = vavoid + (this.transform.position - go.transform.position);
					}

					FlockAI anotherFlock = go.GetComponent<FlockAI>();
					gSpeed = gSpeed + anotherFlock.speed;
				}
			}
		}

		if (groupSize > 0)
		{
			vcentre = vcentre/groupSize + (goalPos - this.transform.position);
			speed =  gSpeed/groupSize;

			Vector3 direction = (vcentre + vavoid) - transform.position;
			if (direction != FishFlock.goalPos)
				transform.rotation = Quaternion.Slerp(transform.rotation,
													Quaternion.LookRotation(direction),
													rotationSpeed * Time.deltaTime);
		}
	}
}
