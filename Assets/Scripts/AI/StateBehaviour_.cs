﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class StateBehaviour_ : MonoBehaviour
{
    public bool alive = true;
    public bool alerted;

    //Behaviour
    public string Behaviour;
    [SerializeField]
    string[] RandomBehaviour;
    string NewBeheaviour;
    [SerializeField]
    public string[] StrafeDirection;
    public string NewStrafeDir;

    //Timers
    float ChooseBehaviourTimer;
    float ChangeStrafeTimer;
    float AlertedTimer;

    //Components
    public NavMeshAgent navMesh;
    public Animator animator;
    AISight sight;
    CharacterStats stats;
    Vector3 InitialPos;
    Transform target;
    Transform player;

    private void Start()
    {
        navMesh = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
        sight = GetComponent<AISight>();
        stats = GetComponent<CharacterStats>();
        InitialPos = transform.position;
        target = GameManager.instance.Player.transform;
        //
        StrafeDirection = new string[]
        {
            "left",
            "right",
        };
    }

    private void Update()
    {
        float ViewRadius = sight.viewRadius;
        float midDistance = 0.75f;
        float targetDistance = Vector3.Distance(transform.position, target.position);

        if (alive)
        {
            //Idle behaviour
            //if (navMesh.velocity == Vector3.zero)
            //{
            //    Behaviour = "idle";
            //    ChangeBehaviour();
            //}

            //Walking behaviour
            if ((targetDistance < ViewRadius * midDistance) && alerted)
            {
                if (ChooseBehaviourTimer <= 0f)
                {
                    RandomBehaviour = new string[]
                    {
                        "walk",
                        "strafe",
                    };
                    Behaviour = GetRandomBehaviour();
                    ChooseBehaviourTimer = 2.5f;
                }
                if (ChooseBehaviourTimer > 0f)
                    ChooseBehaviourTimer -= 1f * Time.deltaTime;
                if (ChooseBehaviourTimer <= 0f)
                    Behaviour = GetRandomBehaviour();
            }

            //Running behaviour
            if ((targetDistance > ViewRadius * midDistance) && alerted)
            {
                if (ChooseBehaviourTimer <= 0)
                {
                    RandomBehaviour = new string[]
                    {
                        "run",
                        //"dashForward",
                    };
                    Behaviour = GetRandomBehaviour();
                    ChooseBehaviourTimer = 2.5f;
                }
                if (ChooseBehaviourTimer > 0)
                    ChooseBehaviourTimer -= 1f * Time.deltaTime;
                if (ChooseBehaviourTimer <= 0)
                    Behaviour = GetRandomBehaviour();
            }

            //If alerted change behaviour
            if (alerted)
                ChangeBehaviour();

            //Reset alerted
            if (sight.visibleTargets.Count == 0)
            {
                if (alerted && AlertedTimer <= 0)
                    AlertedTimer = 10f;
                if (AlertedTimer > 0)
                    AlertedTimer -= 1f * Time.deltaTime;
                if (AlertedTimer <= 0)
                    alerted = false;
            }


        }
        else stats.Die();

    }

    public string GetRandomBehaviour()
    {
        return RandomBehaviour[Random.Range(0, RandomBehaviour.Length)];
    }

    public void ChangeBehaviour()
    {
        //Set offset target for strafing
        Vector3 offsetTarget = (transform.position - target.transform.position).normalized;
        Vector3 dir = Vector3.Cross(offsetTarget, Vector3.up);
        float dist = 90f;

        //Reset animator booleans
        animator.SetBool("isIdle", false);
        animator.SetBool("isWalking", false);
        animator.SetBool("isRunning", false);
        animator.SetBool("isStrafing", false);
        animator.SetBool("strafeLeft", false);
        animator.SetBool("strafeRight", false);

        //Switch behaviour
        switch (Behaviour)
        {
            case "idle":
                Debug.Log(Behaviour);
                animator.SetBool("isIdle", true);
                break;
            case "walk":
                Debug.Log(Behaviour);
                animator.SetBool("isWalking", true);
                navMesh.speed = 1f;
                navMesh.SetDestination(target.position);
                break;
            case "walk2initpos":
                Debug.Log(Behaviour);
                animator.SetBool("isWalking", true);
                navMesh.speed = 2f;
                navMesh.SetDestination(InitialPos);
                break;
            case "run":
                Debug.Log(Behaviour);
                animator.SetBool("isRunning", true);
                navMesh.speed = 6f;
                navMesh.SetDestination(target.position);
                break;
            case "strafe":
                Debug.Log(Behaviour);
                animator.SetBool("isStrafing", true);
                navMesh.speed = 2f;
                if (ChangeStrafeTimer <= 0f)
                {
                    NewStrafeDir = ChangeStrafeDir();
                    ChangeStrafeTimer = Random.Range(2f, 5f);
                }
                if (ChangeStrafeTimer > 0f)
                    ChangeStrafeTimer -= 1f * Time.deltaTime;
                if (ChangeStrafeTimer <= 0f)
                    NewStrafeDir = ChangeStrafeDir();

                switch (NewStrafeDir)
                {
                    case "Left":
                        Debug.Log("Strafe " + NewStrafeDir);
                        animator.SetBool("strafeLeft", true);
                        navMesh.SetDestination(target.position + dir * dist);
                        break;
                    case "Right":
                        Debug.Log("Strafe " + NewStrafeDir);
                        animator.SetBool("strafeRight", true);
                        navMesh.SetDestination(target.position + dir * -dist);
                        break;
                }
                break;
        }
    }

    public string ChangeStrafeDir()
    {
        return StrafeDirection[Random.Range(0, StrafeDirection.Length)];
    }

    public void Dodge()
    {
        if (ChooseBehaviourTimer <= 0f)
        {
            RandomBehaviour = new string[]
            {
                        "strafe",
                        //"dashSide",
            };
            Behaviour = GetRandomBehaviour();
            ChooseBehaviourTimer = 0.25f;
        }
        if (ChooseBehaviourTimer > 0f)
            ChooseBehaviourTimer -= 1f * Time.deltaTime;
        if (ChooseBehaviourTimer <= 0f)
            Behaviour = GetRandomBehaviour();
    }
}
