﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class StateBehaviour : MonoBehaviour
{
    public GameObject torsoBone;
    float viewRadius = 0f;
    float distance;
    float minDistance = 0.7f;
    public float strafeDist;
    //States
    public bool alive = true;
    public bool alerted;
    bool canAttack = true;
    bool canDash = true;
    bool canMove = true;
    bool isDodging;

    //Behaviour
    [SerializeField]
    string[] RandomBehaviour;
    public string _Behaviour;
    string NewBeheaviour;
    [SerializeField]
    string[] StrafeDirection;
    string NewStrafeDir;

    //Cooldown timers
    float changeDirCoolDown;
    float alertedCoolDown;
    float attackCoolDown;
    float attackCoolDownValue = 2f;
    float chooseBehaviour;
    float dashCoolDown;
    float canDashCoolDown;
    float dodgeCoolDown;
    float canMoveCoolDown;

    //Components
    [HideInInspector]
    public Transform target;
    [HideInInspector]
    public NavMeshAgent navMesh;
    [HideInInspector]
    public Vector3 initialPos;
    [HideInInspector]
    public Animator animator;
    [HideInInspector]
    public CharacterStats stats;
    AISight sight;

    void Start()
    {
        animator = GetComponent<Animator>();
        sight = GetComponent<AISight>();
        stats = GetComponent<CharacterStats>();
        target = GameManager.instance.Player.transform;
        navMesh = GetComponent<NavMeshAgent>();
        navMesh.Warp(transform.position);
        initialPos = transform.position;
        StrafeDirection = new string[]
        {
        "Left",
        "Right",
        };
    }

    void Update()
    {
        if (alive)
        {
            viewRadius = sight.viewRadius;
            distance = Vector3.Distance(target.position, transform.position);

            //Target is close behaviour
            if (distance < viewRadius * minDistance && alerted && !isDodging && !animator.GetBool("isHurt") && !animator.GetBool("isDashing") && !animator.GetBool("isAttacking"))
            {
                if (chooseBehaviour <= 0)
                {
                    if (canDash)
                    {
                        RandomBehaviour = new string[]
                        {
                            "walk",
                            "dashForward",
                            "run",
                            "strafe",
                        };
                    }
                    else
                        RandomBehaviour = new string[]
                        {
                            "walk",
                            "run",
                            "strafe",
                        };
                    _Behaviour = GetNewBeheaviour();
                    chooseBehaviour = 2.5f;
                }
                if (chooseBehaviour > 0)
                    chooseBehaviour -= 1f * Time.deltaTime;
                if (chooseBehaviour <= 0)
                    _Behaviour = GetNewBeheaviour();

            }

            //Target is far behaviour
            if (distance > viewRadius * minDistance && alerted && !isDodging && !animator.GetBool("isHurt") && !animator.GetBool("isDashing") && !animator.GetBool("isAttacking"))
            {
                if (chooseBehaviour <= 0)
                {
                    if (canDash)
                    {
                        RandomBehaviour = new string[]
                        {
                            "run",
                            "dashForward",
                        };
                        _Behaviour = GetNewBeheaviour();
                    }
                    else
                        _Behaviour = "run";
                    chooseBehaviour = 2.5f;
                }
                if (chooseBehaviour > 0)
                    chooseBehaviour -= 1f * Time.deltaTime;
                if (chooseBehaviour <= 0)
                    _Behaviour = GetNewBeheaviour();
            }

            //Atack behaviour
            if (distance <= viewRadius * 0.1)
            {
                //Attack
                if (!animator.GetBool("isAttacking") && alerted  && canAttack )
                {
                    _Behaviour = "swordA";
                    AttackTarget();
                }
            }

            //Chase target
            if (alive && alerted && canMove)
            {
                ChangeBehaviour();
            }

            //Return to start pos
            if (alive && !alerted)
            {
                if (transform.position != initialPos)
                    ReturnToInitialPos();
            }

            //Set alerted cooldown
            if (sight.visibleTargets.Count > 0)
                alertedCoolDown = 0;

            //Reset alerted state
            if (sight.visibleTargets.Count == 0)
            {
                if (alerted && alertedCoolDown <= 0f)
                    alertedCoolDown = 10f;
                if (alertedCoolDown > 0f)
                    alertedCoolDown -= 1f * Time.deltaTime;
                if (alertedCoolDown <= 0f)
                    alerted = false;
            }

            //Switch animation states
            if (navMesh.velocity != Vector3.zero && !animator.GetBool("isHurt") && !animator.GetBool("isAttacking"))
            {
                //Reset animator booleans
                animator.SetBool("isIdle", false);
                animator.SetBool("isWalking", false);
                animator.SetBool("isRunning", false);
                animator.SetBool("isStrafing", false);

                switch (_Behaviour)
                {
                    case "walk":
                        animator.SetBool("isWalking", true);
                        break;

                    case "run":
                        animator.SetBool("isRunning", true);
                        break;

                    case "strafe":
                        animator.SetBool("isStrafing", true);
                        break;
                    case "dashForward":
                        if (canDash && !animator.GetBool("isDashing"))
                        {
                            animator.SetBool("isDashing", true);
                        }
                        break;
                    case "dashSide":
                        if (canDash && !animator.GetBool("isDashing"))
                        {
                            animator.SetBool("isDashing", true);
                        }
                        break;
                    case "dashBack":
                        if (canDash && !animator.GetBool("isDashing"))
                        {
                            animator.SetBool("isDashing", true);
                        }
                        break;
                }
            }
            else if (navMesh.velocity == Vector3.zero && !animator.GetBool("isHurt") && !animator.GetBool("isAttacking") && !animator.GetBool("isDashing"))
            {
                animator.SetBool("isIdle", true);
                animator.SetBool("isWalking", false);
                animator.SetBool("isRunning", false);
                animator.SetBool("isStrafing", false);
            }
        }
        else stats.Die();

        //Canmove cooldown
        if (!canMove && canMoveCoolDown <= 0f)
            canMoveCoolDown = 0.5f;
        if (canMoveCoolDown > 0f)
            canMoveCoolDown -= 1f * Time.deltaTime;
        if (canMoveCoolDown <= 0f)
            canMove = true;

        //attack cooldown
        if (!canAttack && attackCoolDown <= 0f)
            attackCoolDown = attackCoolDownValue;
        if (attackCoolDown > 0f)
            attackCoolDown -= 1f * Time.deltaTime;
        if (attackCoolDown <= 0f)
            canAttack = true;

        //Dodge cooldown
        if (isDodging && dodgeCoolDown <= 0f)
            dodgeCoolDown = 1f;
        if (dodgeCoolDown > 0f)
            dodgeCoolDown -= 1f * Time.deltaTime;
        if (dodgeCoolDown <= 0f)
            isDodging = false;

        //Dash cooldown
        if (!canDash && canDashCoolDown <= 0f)
            canDashCoolDown = 2f;
        if (canDashCoolDown > 0f)
            canDashCoolDown -= 1f * Time.deltaTime;
        if (canDashCoolDown <= 0f)
            canDash = true;

        //Dashing stop cooldown
        if (animator.GetBool("isDashing") && dashCoolDown <= 0f)
            dashCoolDown = 0.2f;
        if (dashCoolDown > 0f)
            dashCoolDown -= 1f * Time.deltaTime;
        if (animator.GetBool("isDashing") && dashCoolDown <= 0f)
        {
            navMesh.speed = 0f;
            canMove = false;
            canDash = false;
            animator.SetBool("dashForward", false);
            animator.SetBool("dashLeft", false);
            animator.SetBool("dashRight", false);
            animator.SetBool("dashBack", false);
            animator.SetBool("isDashing", false);
        }
    }

    public void ChangeBehaviour()
    {
        //Set orbit motion
        Vector3 offsetTarget = (transform.position - target.transform.position).normalized;
        Vector3 dir = Vector3.Cross(offsetTarget, Vector3.up);

        if (!animator.GetBool("isHurt") && !animator.GetBool("isDashing") && !animator.GetBool("isAttacking"))
        {
            switch(_Behaviour)
            {
                case "walk":
                    navMesh.speed = 1f;
                    navMesh.SetDestination(target.position);
                    break;
                case "run":
                    navMesh.speed = 6f;
                    navMesh.SetDestination(target.position);
                    break;
                case "strafe":
                    navMesh.speed = 2f;
                    animator.SetBool("strafeLeft", false);
                    animator.SetBool("strafeRight", false);

                    if (changeDirCoolDown <= 0f)
                    {
                        NewStrafeDir = ChangeStrafeDirection();
                        changeDirCoolDown = Random.Range(2f, 5f);
                    }
                    if (changeDirCoolDown > 0f)
                        changeDirCoolDown -= 1f * Time.deltaTime;
                    if (changeDirCoolDown <= 0f)
                        NewStrafeDir = ChangeStrafeDirection();

                    switch (NewStrafeDir)
                    {
                        case "Left":
                            animator.SetBool("strafeLeft", true);
                            navMesh.SetDestination(target.position + dir * strafeDist);
                            break;
                        case "Right":
                            animator.SetBool("strafeRight", true);
                            navMesh.SetDestination(target.position + dir * -strafeDist);
                            break;
                    }
                    break;
                case "dashForward":
                    if (canDash && !animator.GetBool("dashLeft") && !animator.GetBool("dashRight") && !animator.GetBool("dashBack"))
                    {
                        animator.SetBool("dashForward", true);
                        animator.CrossFade("dashForward", 0.25f);
                        navMesh.speed = 50f;
                        navMesh.SetDestination(target.position);
                    }
                    break;
                case "dashSide":
                    if (canDash && !animator.GetBool("dashForward") && !animator.GetBool("dashBack"))
                    {
                        navMesh.speed = 50f;

                        if (changeDirCoolDown <= 0f)
                        {
                            NewStrafeDir = ChangeStrafeDirection();
                            changeDirCoolDown = Random.Range(2f, 5f);
                        }
                        if (changeDirCoolDown > 0f)
                            changeDirCoolDown -= 1f * Time.deltaTime;
                        if (changeDirCoolDown <= 0f)
                            NewStrafeDir = ChangeStrafeDirection();

                        switch (NewStrafeDir)
                        {
                            case "Left":
                                animator.SetBool("dashLeft", true);
                                animator.CrossFade("dashLeft", 0.25f);
                                navMesh.SetDestination(target.position + dir * -strafeDist);
                                break;
                            case "Right":
                                animator.SetBool("dashRight", true);
                                animator.CrossFade("dashRight", 0.25f);
                                navMesh.SetDestination(target.position + dir * strafeDist);
                                break;
                        }
                    }
                    break;
                case "dashBack":
                    if (canDash && !animator.GetBool("dashLeft") && !animator.GetBool("dashRight") && !animator.GetBool("dashForward"))
                    {
                        animator.SetBool("dashBack", true);
                        animator.CrossFade("dashBack", 0.25f);
                        navMesh.speed = 50f;
                        navMesh.SetDestination((transform.position).normalized + Vector3.back * 5f);
                    }
                    break;
                case "swordA":
                    attackCoolDownValue = 2f;
                    navMesh.speed = 0f;
                    break;
            }

        }

    }

    private string ChangeStrafeDirection()
    {
        strafeDist = Random.Range(1f, 30f);
        return StrafeDirection[Random.Range(0, StrafeDirection.Length)];
    }

    private string GetNewBeheaviour()
    {
        return RandomBehaviour[Random.Range(0, RandomBehaviour.Length)];
    }

    public void ReturnToInitialPos()
    {
        if (!animator.GetBool("isHurt") && !animator.GetBool("isDashing") && !animator.GetBool("isAttacking"))
        {
            _Behaviour = "walk";
            navMesh.speed = 1f;
            navMesh.SetDestination(initialPos);
        }
    }

    public void AttackTarget()
    {
        if (!animator.GetBool("isAttacking"))
            animator.SetBool("isAttacking", true);
        switch (_Behaviour)
        {
            case "swordA":
                animator.CrossFade("attack0", 0.25f);
                break;
        }
    }

    public void Dodge()
    {
        isDodging = true;
        if (chooseBehaviour <= 0 && canMove)
        {
            if (canDash && !animator.GetBool("isDashing"))
            {
                if (distance <= viewRadius * 0.2f)
                {
                    RandomBehaviour = new string[]
                    {
                        "dashBack",
                    };
                }
                else
                    RandomBehaviour = new string[]
                    {
                        "dashSide",
                        "strafe",
                    };
                _Behaviour = GetNewBeheaviour();
            }
            else
                _Behaviour = "strafe";
            chooseBehaviour = 0.25f;
        }
        if (chooseBehaviour > 0)
            chooseBehaviour -= 1f * Time.deltaTime;
        if (chooseBehaviour <= 0)
            _Behaviour = GetNewBeheaviour();
    }

    void LookAtTarget()
    {

        transform.LookAt(target);
        // Debug.Log(target.rotation.eulerAngles);
        // torsoBone.transform.Rotate(new Vector3(0, target.transform.rotation.eulerAngles.x, 0));
        // torsoBone.transform.eulerAngles = new Vector3(0, target.transform.rotation.eulerAngles.x, 0);
        // torsoBone.transform.LookAt(new Vector3(0, 0, 0), Vector3.up);
        // torsoBone.transform.Rotate(new Vector3(0, 90, 0));
        // torsoBone.transform.Rotate(new Vector3(0, floatingCamera.transform.rotation.eulerAngles.x, 0));
        // torsoBone.transform.Rotate(new Vector3(0, floatingCamera.transform.rotation.eulerAngles.x, 0));
        // torsoBone.transform.eulerAngles = new Vector3(0, torsoBone.transform.eulerAngles.y, 0);
        // torsoBone.transform.eulerAngles = new Vector3(0, target.transform.rotation.eulerAngles.x, 0);
        // torsoBone.transform.LookAt(new Vector3(target.rotation.x, target.rotation.y, target.rotation.z));
        // torsoBone.transform.eulerAngles = Vector3.zero;
        // Debug.Log(Vector3.Angle(torsoBone.transform.position, target.transform.position));
        // torsoBone.transform.rotation = new Quaternion(90, 90, 90, 90);
        //torsoBone.transform.eulerAngles = new Vector3(-Vector3.Angle(this.transform.position, target.transform.position), torsoBone.transform.eulerAngles.y, torsoBone.transform.eulerAngles.z);
        // torsoBone.transform.LookAt(target.position);
    }

    //If hurt
    private void hurtStart()
    {
        canAttack = false;
        attackCoolDownValue = 4f;
        navMesh.isStopped = true;
    }
    private void hurtEnd()
    {
        navMesh.isStopped = false;
        animator.SetBool("isHurt", false);
    }

    //Start/End attack
    private void attackStart()
    {
        navMesh.isStopped = true;
        canAttack = false;
    }
    private void attackRay()
    {
        if (!animator.GetBool("isHurt"))
        {
            LookAtTarget();
            RaycastHit hit;
            Ray ray = new Ray(transform.position + Vector3.up, transform.forward);
            Debug.DrawRay(ray.origin, ray.direction * 1.5f, Color.blue);
            if (Physics.Raycast(ray.origin, ray.direction, out hit, 1.5f, sight.targetMask))
            {
                PlayerStats targetStats = hit.transform.GetComponent<PlayerStats>();

                if (targetStats != null)
                {
                    Vector3 force = ((-hit.transform.forward * 20f) + (Vector3.up * 20f));
                    hit.rigidbody.AddForce(force, ForceMode.Impulse);
                    hit.transform.LookAt(transform.position);
                    targetStats.animator.CrossFade("Hurt_B", 0.25f);
                    targetStats.TakeDamage(stats.damage);
                }
            }
        }
    }
    private void attackEnd()
    {
        navMesh.isStopped = false;
        animator.SetBool("isAttacking", false);
    }

    //Dash
    private void DashFrontBegin()
    {

    }
    private void invDashFrameEnd()
    {

    }
    private void dashEnd()
    {

    }
    private void DashDirectionalBegin()
    {

    }

    //void OnDrawGizmosSelected()
    //{
    //    Gizmos.color = Color.red;
    //    Gizmos.DrawWireSphere(transform.position, viewRadius);
    //}
}
