﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NewStateBehaviour : MonoBehaviour
{
    //Public values
    public bool IsAlive = true;
    public bool IsAlerted = false;
    public bool CanMove = true;
    public bool CanAttack = true;
    public bool CanDash = true;
    public string ShowCurrState;

    //CoolDowns
    public float DashCool;
    public float CanDashCool;
    public float AttackCool;
    public float AlertCool;

    //Private values
    float DistanceToTarget;
    float MinDistanceToTarget = 0.7f;
    float StrafeDistance;

    //State machine
    public enum AIBehaviour
    {
        Idle,
        Walk,
        ReturnToInitPos,
        Run,
        StrafeLeft,
        StrafeRight,
        Dash,
        DashBack,
        DashLeft,
        DashRight,
        Attack,
    };
    private AIBehaviour currState;
    private string RandomBehaviour;

    //Components
    [HideInInspector]
    public Transform target;
    [HideInInspector]
    public NavMeshAgent agent;
    [HideInInspector]
    public Vector3 initialPos;
    [HideInInspector]
    public Animator animator;
    [HideInInspector]
    public CharacterStats stats;
    AISight sight;
    
    void Start()
    {
        target = GameManager.instance.Player.transform;
        agent = GetComponent<NavMeshAgent>();
        initialPos = transform.position;
        animator = GetComponent<Animator>();
        stats = GetComponent<CharacterStats>();
        sight = GetComponent<AISight>();
    }

    void Update()
    {
        if (IsAlive)
        {
            if (!IsAlerted)
            {
                    currState = AIBehaviour.Idle;
            }

            DistanceToTarget = Vector3.Distance(target.position, transform.position);

            if (DistanceToTarget < sight.viewRadius * MinDistanceToTarget && CanMove && IsAlerted)
            {
                currState = AIBehaviour.Walk;
            }

            if (DistanceToTarget > sight.viewRadius * MinDistanceToTarget && CanMove && IsAlerted)
            {
                currState = AIBehaviour.Run;
            }

            //Change Behaviour
            switch (currState)
            {
                case AIBehaviour.Idle:
                    IdleState();
                    break;
                case AIBehaviour.Walk:
                    WalkState();
                    break;
                case AIBehaviour.ReturnToInitPos:
                    ReturnToInitPosition();
                    break;
                case AIBehaviour.Run:
                    RunState();
                    break;
                case AIBehaviour.Dash:
                    if (CanDash && !animator.GetBool("isDashing"))
                        DashState();
                    break;
            }


            //Dash cooldown
            if (animator.GetBool("isDashing") && DashCool <= 0f)
                DashCool = 0.5f;
            if (DashCool > 0)
                DashCool -= 1 * Time.deltaTime;
            if (animator.GetBool("isDashing") && DashCool <= 0)
            {
                CanMove = false;
                CanDash = false;
                agent.isStopped = true;
                animator.SetBool("isDashing", false);
            }

            //Can dash cooldown
            if (!CanDash && CanDashCool <= 0f)
                CanDashCool = 1.1f;
            if (CanDashCool > 0f)
                CanDashCool -= 1 * Time.deltaTime;
            if (!CanDash && CanDashCool <= 0)
            {
                CanMove = true;
                CanDash = true;
            }

            //Show current state
            ShowCurrState = currState.ToString();
        }
        else
            stats.Die();
    }


    //Manage Behaviours
    string GetRandomBehaviour(string[] BehaviourList)
    {
        return BehaviourList[Random.Range(0, BehaviourList.Length)];
    }

    public void Dodge()
    {

    }

    void IdleState()
    {
        //Reset animator booleans
        animator.SetBool("isIdle", false);
        animator.SetBool("isWalking", false);
        animator.SetBool("isRunning", false);
        animator.SetBool("isStrafing", false);
        //Set new animator bool
        animator.SetBool("isIdle", true);
        agent.speed = 0f;
    }

    void WalkState()
    {
        //Reset animator booleans
        animator.SetBool("isIdle", false);
        animator.SetBool("isWalking", false);
        animator.SetBool("isRunning", false);
        animator.SetBool("isStrafing", false);
        //Set new animator bool
        animator.SetBool("isWalking", true);
        agent.isStopped = false;
        agent.speed = 1f;
        agent.SetDestination(target.position);
    }

    void ReturnToInitPosition()
    {
        //Reset animator booleans
        animator.SetBool("isIdle", false);
        animator.SetBool("isWalking", false);
        animator.SetBool("isRunning", false);
        animator.SetBool("isStrafing", false);
        //Set new animator bool
        animator.SetBool("isWalking", true);
        agent.isStopped = false;
        agent.speed = 1f;
        agent.SetDestination(initialPos);
    }

    void RunState()
    {
        //Reset animator booleans
        animator.SetBool("isIdle", false);
        animator.SetBool("isWalking", false);
        animator.SetBool("isRunning", false);
        animator.SetBool("isStrafing", false);
        //Set new animator bool
        animator.SetBool("isRunning", true);
        agent.isStopped = false;
        agent.speed = 6f;
        agent.SetDestination(target.position);
    }

    void DashState()
    {
        //Reset animator booleans
        animator.SetBool("isIdle", false);
        animator.SetBool("isWalking", false);
        animator.SetBool("isRunning", false);
        animator.SetBool("isStrafing", false);
        //Set new animator bool
        animator.SetBool("isDashing", true);
        animator.Play("dashForward", 0, Time.deltaTime);
        agent.isStopped = false;
        agent.speed = 50f;
        agent.SetDestination(target.position);
    }

    void temp()
    {
        string[] BehaviourList;
        BehaviourList = new string[]
        {
                    "Walk",
                    "Dash",
        };

        if (RandomBehaviour == null)
            RandomBehaviour = GetRandomBehaviour(BehaviourList);
        currState = (AIBehaviour)System.Enum.Parse(typeof(AIBehaviour), RandomBehaviour);
    }

    //If hurt
    private void hurtStart()
    {
        CanAttack = false;
        AttackCool = 4f;
        agent.isStopped = true;
    }
    private void hurtEnd()
    {
        agent.isStopped = false;
        animator.SetBool("isHurt", false);
    }
}