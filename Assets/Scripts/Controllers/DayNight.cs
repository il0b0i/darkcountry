﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DayNight : MonoBehaviour {

	[Header("Time of day and speed")]
	//what's the current time
	[Range(0f, 24.0f)]
	public float currentTime = 0.0f;
	//Speed of the cycle (if you set this to 1 the one hour in the cycle will pass in 1 real life second)
	public float daySpeedMultiplier = 0.01f;
	public string timeString = "00:00 AM";

	[Header("Sun/Moon lights")]
	//Set the sun/moon lights
	public Light sunLight;
	public Light moonLight;

	[Header("Cloud sphere")]
	//Set cloud sphere (if any) and the camera to attach to
	private Camera mainCamera;
	public GameObject cloudSphere;

	[Header("Materials")]
	//Set materials that are going to be affected **NOTE: you must also manually assing the variables of the shaders that are going to be affected
	public Material waterMat;
	public Material cloud_1;
	public Material cloud_2;
	public Material skyBoxMat;

	[Header("Assign other values")]
	//assing day times, when does each start?
	public float sunriseAt = 5.0f;
	public float dayAt = 5.5f;
	public float sunsetAt = 18.0f;
	public float nightAt = 18.5f;
	public float sunriseIntensity = 2.0f;
	public float dayIntensity = 7.0f;
	public float sunsetIntensity = 3.0f;
	public float nightIntensity = 1.0f;

	//Set transition speed between the daytime values
	public float smoothTransition = 0.1f;
	//Z value of the sun's rotation
	public float sunDirection = -90f;
	//control intensity of sun and ambient?
	public bool controlIntensity = true;

	[HideInInspector] 
	//check for night time
	public bool nightTime = false;

	//x rotation value of the light
	private float xValueOfSun = 90.0f;

	[Header("Sunrise colors")]
	//Set sunrise colors
	[ColorUsageAttribute(true,true)] public Color sunriseColor;
	[ColorUsageAttribute(true,true)] public Color fogSunriseColor;
	[ColorUsageAttribute(true,true)] public Color waterBCSunriseColor;
	[ColorUsageAttribute(true,true)] public Color waterRESunriseColor;
	[ColorUsageAttribute(true,true)] public Color cloud1SunriseColor;
	[ColorUsageAttribute(true,true)] public Color cloud2SunriseColor;
	[ColorUsageAttribute(true,true)] public Color skyBoxTopSunriseColor;
	[ColorUsageAttribute(true,true)] public Color skyBoxHorizonSunriseColor;
	[ColorUsageAttribute(true,true)] public Color skyBoxBottomSunriseColor;

	[Header("Day colors")]
	//Set day colors
	[ColorUsageAttribute(true,true)] public Color dayColor;
	[ColorUsageAttribute(true,true)] public Color fogDayColor;
	[ColorUsageAttribute(true,true)] public Color waterBCDayColor;
	[ColorUsageAttribute(true,true)] public Color waterREDayColor;
	[ColorUsageAttribute(true,true)] public Color cloud1DayColor;
	[ColorUsageAttribute(true,true)] public Color cloud2DayColor;
	[ColorUsageAttribute(true,true)] public Color skyBoxTopDayColor;
	[ColorUsageAttribute(true,true)] public Color skyBoxHorizonDayColor;
	[ColorUsageAttribute(true,true)] public Color skyBoxBottomDayColor;

	[Header("Sunset colors")]
	//Set sunset colors
	[ColorUsageAttribute(true,true)] public Color sunsetColor;
	[ColorUsageAttribute(true,true)] public Color fogSunsetColor;
	[ColorUsageAttribute(true,true)] public Color waterBCSunsetColor;
	[ColorUsageAttribute(true,true)] public Color waterRESunsetColor;
	[ColorUsageAttribute(true,true)] public Color cloud1SunsetColor;
	[ColorUsageAttribute(true,true)] public Color cloud2SunsetColor;
	[ColorUsageAttribute(true,true)] public Color skyBoxTopSunsetColor;
	[ColorUsageAttribute(true,true)] public Color skyBoxHorizonSunsetColor;
	[ColorUsageAttribute(true,true)] public Color skyBoxBottomSunsetColor;

	[Header("Night colors")]
	//Set night colors
	[ColorUsageAttribute(true,true)] public Color nightColor;
	[ColorUsageAttribute(true,true)] public Color fogNightColor;
	[ColorUsageAttribute(true,true)] public Color waterBCNightColor;
	[ColorUsageAttribute(true,true)] public Color waterRENightColor;
	[ColorUsageAttribute(true,true)] public Color cloud1NightColor;
	[ColorUsageAttribute(true,true)] public Color cloud2NightColor;
	[ColorUsageAttribute(true,true)] public Color skyBoxTopNightColor;
	[ColorUsageAttribute(true,true)] public Color skyBoxHorizonNightColor;
	[ColorUsageAttribute(true,true)] public Color skyBoxBottomNightColor;

	void Start()
	{
		mainCamera = Camera.main;
        SetupLight();
	}
	
	void Update () {
		//increment time
		currentTime += Time.deltaTime*daySpeedMultiplier;
        //FOR TESTING Mouse scroll control time
        if (Input.GetAxis("Mouse ScrollWheel") > 0f)
        {
            currentTime += 0.5f;
        }
        else if (Input.GetAxis("Mouse ScrollWheel") < 0f)
        {
            currentTime -= 0.5f;
        }
        //reset time
        if (currentTime >= 24.0f) {
			currentTime %= 24.0f;
		}
        else if (currentTime <= 0.0f)
        {
            currentTime = 24.0f;
        }
        //Check for sunlight
        if (sunLight) {
			ControlLight();
		}
	
		//Clouds follow camera
		if (cloudSphere && mainCamera)
		{
			cloudSphere.transform.position = mainCamera.transform.position;
		}

		//Gets The timeString;
		CalculateTime ();

	}
	
	//Set up color and lights from editor
	public void SetupLight() {
		//Sunrise
		if (currentTime >= sunriseAt && currentTime <= dayAt)
		{
			sunLight.color = sunriseColor;
			//RenderSettings.ambientLight = sunriseColor;
			RenderSettings.fogColor = fogSunriseColor;
			waterMat.SetColor("_BaseColor", waterBCSunriseColor);
			waterMat.SetColor("_ReflectionColor",waterRESunriseColor);
			cloud_1.SetColor("_CloudColor", cloud1SunriseColor);
			cloud_2.SetColor("_CloudColor", cloud2SunriseColor);
			skyBoxMat.SetColor("_Color1", skyBoxTopSunriseColor);
			skyBoxMat.SetColor("_Color2", skyBoxHorizonSunriseColor);
			skyBoxMat.SetColor("_Color3", skyBoxBottomSunriseColor);

			RenderSettings.reflectionIntensity = 0.5f;
            RenderSettings.ambientIntensity = 0.5f;
			sunLight.intensity = sunriseIntensity;
			moonLight.intensity = 0.0f;

		}
		//Day
		else if (currentTime >= dayAt && currentTime <= sunsetAt)
		{
			sunLight.color = dayColor;
			//RenderSettings.ambientLight = dayColor;
			RenderSettings.fogColor = fogDayColor;
			waterMat.SetColor("_BaseColor", waterBCDayColor);
			waterMat.SetColor("_ReflectionColor",waterREDayColor);
			cloud_1.SetColor("_CloudColor", cloud1DayColor);
			cloud_2.SetColor("_CloudColor", cloud2DayColor);
			skyBoxMat.SetColor("_Color1", skyBoxTopDayColor);
			skyBoxMat.SetColor("_Color2", skyBoxHorizonDayColor);
			skyBoxMat.SetColor("_Color3", skyBoxBottomDayColor);

			RenderSettings.reflectionIntensity = 1.0f;
            RenderSettings.ambientIntensity = 1.0f;
            sunLight.intensity = dayIntensity;
			moonLight.intensity = 0.0f;
		}
		//Sunset
		else if (currentTime >= sunsetAt && currentTime <= nightAt)
		{
			sunLight.color = sunsetColor;
			//RenderSettings.ambientLight = sunsetColor;
			RenderSettings.fogColor = fogSunsetColor;
			waterMat.SetColor("_BaseColor", waterBCSunsetColor);
			waterMat.SetColor("_ReflectionColor",waterRESunsetColor);
			cloud_1.SetColor("_CloudColor", cloud1SunsetColor);
			cloud_2.SetColor("_CloudColor", cloud2SunsetColor);
			skyBoxMat.SetColor("_Color1", skyBoxTopSunsetColor);
			skyBoxMat.SetColor("_Color2", skyBoxHorizonSunsetColor);
			skyBoxMat.SetColor("_Color3", skyBoxBottomSunsetColor);

			RenderSettings.reflectionIntensity = 0.5f;
            RenderSettings.ambientIntensity = 0.5f;
            sunLight.intensity = sunsetIntensity;
			moonLight.intensity = 0.0f;
		}
		//Night
		else if (currentTime >= nightAt && currentTime <= 24.0f || currentTime <= sunriseAt)
		{
			sunLight.color = nightColor;
			//RenderSettings.ambientLight = nightColor;
			RenderSettings.fogColor = fogNightColor;
			waterMat.SetColor("_BaseColor", waterBCNightColor);
			waterMat.SetColor("_ReflectionColor",waterRENightColor);
			cloud_1.SetColor("_CloudColor", cloud1NightColor);
			cloud_2.SetColor("_CloudColor", cloud2NightColor);
			skyBoxMat.SetColor("_Color1", skyBoxTopNightColor);
			skyBoxMat.SetColor("_Color2", skyBoxHorizonNightColor);
			skyBoxMat.SetColor("_Color3", skyBoxBottomNightColor);

			RenderSettings.reflectionIntensity = 0.0f;
            RenderSettings.ambientIntensity = 0.0f;
            sunLight.intensity = 0.0f;
			moonLight.intensity = nightIntensity;
		}
	}


	public void ControlLight() {
		//Rotate light
		xValueOfSun = -(90.0f+currentTime*15.0f);
		sunLight.transform.localRotation = Quaternion.Euler(xValueOfSun, sunDirection, 0);
		//reset angle
		if (xValueOfSun >= 360.0f) {
			xValueOfSun = 0.0f;
		}

		//Sunrise
		if (currentTime >= sunriseAt && currentTime <= dayAt)
		{
			sunrise();
		}
		//Day
		else if (currentTime >= dayAt && currentTime <= sunsetAt)
		{
			day();
		}
		//Sunset
		else if (currentTime >= sunsetAt && currentTime <= nightAt)
		{
			sunset();
		}
		//Night
		else if (currentTime >= nightAt && currentTime <= 24.0f || currentTime <= sunriseAt)
		{
			night();
		}

	}

	//Set values for color according to day time
	public void sunrise()
	{
			nightTime = false;
			sunLight.color = Color.Lerp(sunLight.color, sunriseColor, Time.deltaTime * (smoothTransition + daySpeedMultiplier));
			//RenderSettings.ambientLight = Color.Lerp(RenderSettings.ambientLight, sunriseColor, Time.deltaTime * (smoothTransition + daySpeedMultiplier));
            RenderSettings.fogColor = Color.Lerp(RenderSettings.fogColor , fogSunriseColor, Time.deltaTime * (smoothTransition + daySpeedMultiplier));
			waterMat.SetColor("_BaseColor",Color.Lerp(waterMat.GetColor("_BaseColor"), waterBCSunriseColor, Time.deltaTime * (smoothTransition + daySpeedMultiplier)));
			waterMat.SetColor("_ReflectionColor",Color.Lerp(waterMat.GetColor("_ReflectionColor"), waterRESunriseColor, Time.deltaTime * (smoothTransition + daySpeedMultiplier)));
			cloud_1.SetColor("_CloudColor",Color.Lerp(cloud_1.GetColor("_CloudColor"), cloud1SunriseColor, Time.deltaTime * (smoothTransition + daySpeedMultiplier)));
			cloud_2.SetColor("_CloudColor",Color.Lerp(cloud_2.GetColor("_CloudColor"), cloud2SunriseColor, Time.deltaTime * (smoothTransition + daySpeedMultiplier)));
			skyBoxMat.SetColor("_Color1",Color.Lerp(skyBoxMat.GetColor("_Color1"), skyBoxTopSunriseColor, Time.deltaTime * (smoothTransition + daySpeedMultiplier)));
			skyBoxMat.SetColor("_Color2",Color.Lerp(skyBoxMat.GetColor("_Color2"), skyBoxHorizonSunriseColor, Time.deltaTime * (smoothTransition + daySpeedMultiplier)));
			skyBoxMat.SetColor("_Color3",Color.Lerp(skyBoxMat.GetColor("_Color3"), skyBoxBottomSunriseColor, Time.deltaTime * (smoothTransition + daySpeedMultiplier)));
			
			//this controls the intensity of sun/moon lights and ambience reflections intensity
			RenderSettings.reflectionIntensity = Mathf.MoveTowards(RenderSettings.reflectionIntensity, 0.5f, Time.deltaTime * (smoothTransition + daySpeedMultiplier));
			RenderSettings.ambientIntensity = Mathf.MoveTowards(RenderSettings.ambientIntensity, 0.5f,Time.deltaTime * (smoothTransition + daySpeedMultiplier));
            sunLight.intensity = Mathf.MoveTowards(sunLight.intensity,sunriseIntensity,Time.deltaTime * (smoothTransition + daySpeedMultiplier));
			moonLight.intensity = Mathf.MoveTowards(moonLight.intensity,0.0f,Time.deltaTime * (smoothTransition + daySpeedMultiplier));	}
	public void day()
	{
			nightTime = false;
			sunLight.color = Color.Lerp(sunLight.color, dayColor, Time.deltaTime * (smoothTransition + daySpeedMultiplier));
			//RenderSettings.ambientLight = Color.Lerp(RenderSettings.ambientLight, dayColor, Time.deltaTime * (smoothTransition + daySpeedMultiplier));
			RenderSettings.fogColor = Color.Lerp(RenderSettings.fogColor , fogDayColor, Time.deltaTime * (smoothTransition + daySpeedMultiplier));
			waterMat.SetColor("_BaseColor",Color.Lerp(waterMat.GetColor("_BaseColor"), waterBCDayColor, Time.deltaTime * (smoothTransition + daySpeedMultiplier)));
			waterMat.SetColor("_ReflectionColor",Color.Lerp(waterMat.GetColor("_ReflectionColor"), waterREDayColor, Time.deltaTime * (smoothTransition + daySpeedMultiplier)));
			cloud_1.SetColor("_CloudColor",Color.Lerp(cloud_1.GetColor("_CloudColor"), cloud1DayColor, Time.deltaTime * (smoothTransition + daySpeedMultiplier)));
			cloud_2.SetColor("_CloudColor",Color.Lerp(cloud_2.GetColor("_CloudColor"), cloud2DayColor, Time.deltaTime * (smoothTransition + daySpeedMultiplier)));
			skyBoxMat.SetColor("_Color1",Color.Lerp(skyBoxMat.GetColor("_Color1"), skyBoxTopDayColor, Time.deltaTime * (smoothTransition + daySpeedMultiplier)));
			skyBoxMat.SetColor("_Color2",Color.Lerp(skyBoxMat.GetColor("_Color2"), skyBoxHorizonDayColor, Time.deltaTime * (smoothTransition + daySpeedMultiplier)));
			skyBoxMat.SetColor("_Color3",Color.Lerp(skyBoxMat.GetColor("_Color3"), skyBoxBottomDayColor, Time.deltaTime * (smoothTransition + daySpeedMultiplier)));
			
			//this controls the intensity of sun/moon lights and ambience reflections intensity
			RenderSettings.reflectionIntensity = Mathf.MoveTowards(RenderSettings.reflectionIntensity,1.0f,Time.deltaTime * (smoothTransition + daySpeedMultiplier));
			RenderSettings.ambientIntensity = Mathf.MoveTowards(RenderSettings.ambientIntensity, 1.0f,Time.deltaTime * (smoothTransition + daySpeedMultiplier));
            sunLight.intensity = Mathf.MoveTowards(sunLight.intensity,dayIntensity,Time.deltaTime * (smoothTransition + daySpeedMultiplier));
			moonLight.intensity = Mathf.MoveTowards(moonLight.intensity,0.0f,Time.deltaTime * (smoothTransition + daySpeedMultiplier));	
	}
	public void sunset()
	{
			nightTime = false;
			sunLight.color = Color.Lerp(sunLight.color, sunsetColor, Time.deltaTime * (smoothTransition + daySpeedMultiplier));
			//RenderSettings.ambientLight = Color.Lerp(RenderSettings.ambientLight, sunsetColor, Time.deltaTime * (smoothTransition + daySpeedMultiplier));
			RenderSettings.fogColor = Color.Lerp(RenderSettings.fogColor , fogSunsetColor, Time.deltaTime * (smoothTransition + daySpeedMultiplier));
			waterMat.SetColor("_BaseColor",Color.Lerp(waterMat.GetColor("_BaseColor"), waterBCSunsetColor, Time.deltaTime * (smoothTransition + daySpeedMultiplier)));	
			waterMat.SetColor("_ReflectionColor",Color.Lerp(waterMat.GetColor("_ReflectionColor"), waterRESunsetColor, Time.deltaTime * (smoothTransition + daySpeedMultiplier)));
			cloud_1.SetColor("_CloudColor",Color.Lerp(cloud_1.GetColor("_CloudColor"), cloud1SunsetColor, Time.deltaTime * (smoothTransition + daySpeedMultiplier)));
			cloud_2.SetColor("_CloudColor",Color.Lerp(cloud_2.GetColor("_CloudColor"), cloud2SunsetColor, Time.deltaTime * (smoothTransition + daySpeedMultiplier)));
			skyBoxMat.SetColor("_Color1",Color.Lerp(skyBoxMat.GetColor("_Color1"), skyBoxTopSunsetColor, Time.deltaTime * (smoothTransition + daySpeedMultiplier)));
			skyBoxMat.SetColor("_Color2",Color.Lerp(skyBoxMat.GetColor("_Color2"), skyBoxHorizonSunsetColor, Time.deltaTime * (smoothTransition + daySpeedMultiplier)));
			skyBoxMat.SetColor("_Color3",Color.Lerp(skyBoxMat.GetColor("_Color3"), skyBoxBottomSunsetColor, Time.deltaTime * (smoothTransition + daySpeedMultiplier)));
			
			//this controls the intensity of sun/moon lights and ambience reflections intensity
			RenderSettings.reflectionIntensity = Mathf.MoveTowards(RenderSettings.reflectionIntensity,0.5f,Time.deltaTime * (smoothTransition + daySpeedMultiplier));
            RenderSettings.ambientIntensity = Mathf.MoveTowards(RenderSettings.ambientIntensity, 0.5f, Time.deltaTime * (smoothTransition + daySpeedMultiplier));
            sunLight.intensity = Mathf.MoveTowards(sunLight.intensity,sunsetIntensity,Time.deltaTime * (smoothTransition + daySpeedMultiplier));
			moonLight.intensity = Mathf.MoveTowards(moonLight.intensity,0.0f,Time.deltaTime * (smoothTransition + daySpeedMultiplier));
	}
	public void night()
	{
			nightTime = true;
			sunLight.color = Color.Lerp(sunLight.color, nightColor, Time.deltaTime * (smoothTransition + daySpeedMultiplier));
			//RenderSettings.ambientLight = Color.Lerp(RenderSettings.ambientLight, nightColor, Time.deltaTime * (smoothTransition + daySpeedMultiplier));
			RenderSettings.fogColor = Color.Lerp(RenderSettings.fogColor , fogNightColor, Time.deltaTime * (smoothTransition + daySpeedMultiplier));
			waterMat.SetColor("_BaseColor",Color.Lerp(waterMat.GetColor("_BaseColor"), waterBCNightColor, Time.deltaTime * (smoothTransition + daySpeedMultiplier)));
			waterMat.SetColor("_ReflectionColor",Color.Lerp(waterMat.GetColor("_ReflectionColor"), waterRENightColor, Time.deltaTime + daySpeedMultiplier));
			cloud_1.SetColor("_CloudColor",Color.Lerp(cloud_1.GetColor("_CloudColor"), cloud1NightColor, Time.deltaTime * (smoothTransition + daySpeedMultiplier)));
			cloud_2.SetColor("_CloudColor",Color.Lerp(cloud_2.GetColor("_CloudColor"), cloud2NightColor, Time.deltaTime * (smoothTransition + daySpeedMultiplier)));
			skyBoxMat.SetColor("_Color1",Color.Lerp(skyBoxMat.GetColor("_Color1"), skyBoxTopNightColor, Time.deltaTime * (smoothTransition + daySpeedMultiplier)));
			skyBoxMat.SetColor("_Color2",Color.Lerp(skyBoxMat.GetColor("_Color2"), skyBoxHorizonNightColor, Time.deltaTime * (smoothTransition + daySpeedMultiplier)));
			skyBoxMat.SetColor("_Color3",Color.Lerp(skyBoxMat.GetColor("_Color3"), skyBoxBottomNightColor, Time.deltaTime * (smoothTransition + daySpeedMultiplier)));

			//this controls the intensity of sun/moon lights and ambience reflections intensity
			RenderSettings.reflectionIntensity = Mathf.MoveTowards(RenderSettings.reflectionIntensity,0.0f,Time.deltaTime * (smoothTransition + daySpeedMultiplier));
			RenderSettings.ambientIntensity = Mathf.MoveTowards(RenderSettings.ambientIntensity, 0.0f,Time.deltaTime * (smoothTransition + daySpeedMultiplier));
            sunLight.intensity = Mathf.MoveTowards(sunLight.intensity,0.0f,Time.deltaTime * (smoothTransition + daySpeedMultiplier));
			moonLight.intensity = Mathf.MoveTowards(moonLight.intensity,nightIntensity,Time.deltaTime * (smoothTransition + daySpeedMultiplier));
	}

	public void CalculateTime() 
	{
		//Is it am of pm?
		string AMPM = "";
		float minutes = ((currentTime) - (Mathf.Floor(currentTime)))*60.0f;
		if (currentTime <= 12.0f) {
			AMPM = "AM";

		} else {
			AMPM = "PM";
		}
		//Make the final string
		timeString = Mathf.Floor(currentTime).ToString() + " : " + minutes.ToString("F0") + " "+AMPM ;

	}

}
