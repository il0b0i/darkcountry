﻿using UnityEngine;
using UnityEngine.UI;

public class OverheadUIController : MonoBehaviour
{
  public Canvas healthSlotCanvas;
  public GameObject healthSlot;
  public Sprite healthSlotActive;
  public Sprite healthSlotInactive;
  public Canvas healthBarCanvas;
  public GameObject healthBar;
  public Sprite healthBarActive;
  public Sprite healthBarInactive;
  private PlayerController playerController;
  private GameObject[] healthSlotObjects = new GameObject[20];
  private GameObject[] healthBarObjects = new GameObject[9];

  void Start()
  {
    playerController = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
    float healthBarCanvasWidth = healthBarCanvas.GetComponent<RectTransform>().rect.width;
    float healthBarWidth = (healthBarCanvasWidth - ((playerController.maxHealth - 1) * 4)) / playerController.maxHealth;
    float healthSlotWidth = 16f;

    for (int i = 0; i < playerController.maxHealth; i++)
    {
      healthBarObjects[i] = GameObject.Instantiate(healthBar, healthBarCanvas.transform);
      healthBarObjects[i].GetComponent<RectTransform>().sizeDelta = new Vector2(healthBarWidth, 4);
      healthBarObjects[i].GetComponent<RectTransform>().anchoredPosition = new Vector2((healthBarWidth + 4) * i, -2);
      healthBarObjects[i].GetComponent<Image>().sprite = healthBarInactive;

      if ((playerController.health - 1) >= i)
      {
        healthBarObjects[i].GetComponent<Image>().sprite = healthBarActive;
      }
    }

    for (int i = 0; i < playerController.maxRegenSlots; i++)
    {
      healthSlotObjects[i] = GameObject.Instantiate(healthSlot, healthSlotCanvas.transform);
      healthSlotObjects[i].GetComponent<RectTransform>().sizeDelta = new Vector2(healthSlotWidth, 4);
      healthSlotObjects[i].GetComponent<RectTransform>().anchoredPosition = new Vector2((healthSlotWidth + 2) * i, -2);
      healthSlotObjects[i].GetComponent<Image>().sprite = healthSlotInactive;

      if ((playerController.regenSlots - 1) >= i)
      {
        healthSlotObjects[i].GetComponent<Image>().sprite = healthSlotActive;
      }
    }

    healthSlotCanvas.GetComponent<RectTransform>().sizeDelta = new Vector2((playerController.maxRegenSlots * healthSlotWidth + ((playerController.maxRegenSlots - 1) * 2)), 4);
  }

  void Update()
  {
  }
}
