﻿using Cinemachine;
using UnityEngine;

public class LockOnTarget : MonoBehaviour
{
    CinemachineFreeLook c_VirtualCam;
    Transform defaultAimPoint;
    public Transform target;

    void Start()
    {
        c_VirtualCam = GetComponent<CinemachineFreeLook>();
        defaultAimPoint = c_VirtualCam.m_LookAt;
    }

    void Update()
    {
        if (target != null)
        {
            c_VirtualCam.m_LookAt = target;
        }
        else
            c_VirtualCam.m_LookAt = defaultAimPoint;
    }
}
