﻿using UnityEngine;
using UnityEngine.UI;

public class HudController : MonoBehaviour
{
    GameObject player;
    public GameObject crossHairOb;
    Image crossHair;

    void Start()
    {
        player = GameManager.instance.Player;
        crossHair = crossHairOb.GetComponent<Image>();
    }

    void Update()
    {
        var playerAnimator = player.GetComponent<Animator>();
        var showCrossHair = playerAnimator.GetBool("isAiming");

        if (showCrossHair)
            crossHair.CrossFadeAlpha(1f, 1 * Time.deltaTime, false); 
        else
            crossHair.CrossFadeAlpha(0f, 1 * Time.deltaTime, false);
    }
}
