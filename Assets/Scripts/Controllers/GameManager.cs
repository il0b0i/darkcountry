﻿using UnityEngine;

public class GameManager : MonoBehaviour {

    #region Singleton

    public static GameManager instance;

    void Awake()
    {
        instance = this;
    }

    #endregion

    public GameObject Player;
    public GameObject PlayerTorsoBone;
    public GameObject playerLeftGun;
    public GameObject playerRightGun;
    public GameObject DayCycleController;
}
