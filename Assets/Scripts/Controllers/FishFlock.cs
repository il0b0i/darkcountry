﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishFlock : MonoBehaviour {

	public GameObject fishManager;
	public GameObject fishPrefab;
	//
	public static int tankSize = 10; 
	static int numFish = 10; 
	//
	public static GameObject[] allFish = new GameObject[numFish]; 

	public static Vector3 goalPos = Vector3.zero;

	// Use this for initialization
	void Start () {
		goalPos = fishManager.transform.position;
		for(int i = 0; i < numFish; i++)
		{
			Vector3 pos = fishManager.transform.position + new Vector3(Random.Range(-tankSize,tankSize),
						Random.Range(-tankSize,tankSize),	
						Random.Range(-tankSize,tankSize));

			allFish[i] = (GameObject) Instantiate(fishPrefab, pos, Quaternion.identity);
		}
	}

	// Update is called once per frame
	void Update () {
		if (Random.Range(0,10000) < 50)
		{
			goalPos = new Vector3(fishManager.transform.position.x + Random.Range(-tankSize,tankSize),
						fishManager.transform.position.y + Random.Range(-4, 4),	
						fishManager.transform.position.z + Random.Range(-tankSize,tankSize));
		}
	}
}

