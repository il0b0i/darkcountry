﻿using UnityEngine;

public class CamController : MonoBehaviour
{
    public GameObject[] vCamera;
    public Transform target;

    PlayerController playerController;

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        playerController = GetComponent<PlayerController>();
    }

    void Update()
    {
        if (playerController.animator.GetBool("isAiming"))
        {
            vCamera[0].SetActive(false);
            if (playerController.aimFocused)
            {
                vCamera[1].SetActive(false);
                vCamera[2].SetActive(true);
            }
            else
            {
                vCamera[1].SetActive(true);
                vCamera[2].SetActive(false);
            }
        }
        else
        {
            target = null;
            vCamera[0].SetActive(true);
            vCamera[1].SetActive(false);
            vCamera[2].SetActive(false);
        }
    }
}
