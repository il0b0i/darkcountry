﻿using UnityEngine;

public class AmbientDayNightAudioManager : MonoBehaviour {

	public GameObject dayAmbientSource;
	public GameObject nightAmbientSource;
	public bool ambientDay = false;
	public bool ambientNight = false;
	AudioSource audioSourceDay;
	AudioSource audioSourceNight;
	DayNight dayNightController;

	void Start()
	{
		audioSourceDay = dayAmbientSource.GetComponent<AudioSource>();
		audioSourceNight = nightAmbientSource.GetComponent<AudioSource>();
		dayNightController = GameManager.instance.DayCycleController.GetComponent<DayNight>();
	}
	
	// Update is called once per frame
	void Update () 
	{

		if (dayNightController.nightTime && !ambientNight)
		{
			ambientDay = false;
			ambientNight = true;
			audioSourceNight.Play();
		}
		else if (!dayNightController.nightTime && !ambientDay)
		{
			ambientDay = true;
			ambientNight = false;
			audioSourceDay.Play();
		}

		if (ambientDay)
		{
			//
			if (audioSourceNight.volume >= 0.0f)
			{
				audioSourceNight.volume -= (Time.deltaTime/10);
			}
			//
			if (audioSourceDay.volume <= 1.0f) 
			{
				audioSourceDay.volume += (Time.deltaTime/10);	
			}
		}
		else if (ambientNight)
		{
			//
			if (audioSourceDay.volume >= 0.0f)
			{
				audioSourceDay.volume -= (Time.deltaTime/10);
			}
			//
			if (audioSourceNight.volume <= 1.0f) 
			{
				audioSourceNight.volume += (Time.deltaTime/10);	
			}
		}
	}
}
