%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: PlayerMask
  m_Mask: 00000000010000000100000000000000000000000100000001000000010000000100000000000000000000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Armature
    m_Weight: 1
  - m_Path: Armature/hip
    m_Weight: 0
  - m_Path: Armature/hip/abs
    m_Weight: 0
  - m_Path: Armature/hip/abs/chest
    m_Weight: 1
  - m_Path: Armature/hip/abs/chest/neck
    m_Weight: 1
  - m_Path: Armature/hip/abs/chest/neck/head
    m_Weight: 0
  - m_Path: Armature/hip/abs/chest/shoulder_l
    m_Weight: 1
  - m_Path: Armature/hip/abs/chest/shoulder_l/upperarm_l
    m_Weight: 1
  - m_Path: Armature/hip/abs/chest/shoulder_l/upperarm_l/lowerarm_l
    m_Weight: 1
  - m_Path: Armature/hip/abs/chest/shoulder_l/upperarm_l/lowerarm_l/hand_l
    m_Weight: 1
  - m_Path: Armature/hip/abs/chest/shoulder_l/upperarm_l/lowerarm_l/hand_l/index_01_l
    m_Weight: 1
  - m_Path: Armature/hip/abs/chest/shoulder_l/upperarm_l/lowerarm_l/hand_l/index_01_l/index_02_l
    m_Weight: 1
  - m_Path: Armature/hip/abs/chest/shoulder_l/upperarm_l/lowerarm_l/hand_l/mid_01_l
    m_Weight: 1
  - m_Path: Armature/hip/abs/chest/shoulder_l/upperarm_l/lowerarm_l/hand_l/mid_01_l/mid_02_l
    m_Weight: 1
  - m_Path: Armature/hip/abs/chest/shoulder_l/upperarm_l/lowerarm_l/hand_l/pink_01_l
    m_Weight: 1
  - m_Path: Armature/hip/abs/chest/shoulder_l/upperarm_l/lowerarm_l/hand_l/pink_01_l/pink_01_l_001
    m_Weight: 1
  - m_Path: Armature/hip/abs/chest/shoulder_l/upperarm_l/lowerarm_l/hand_l/ring_01_l
    m_Weight: 1
  - m_Path: Armature/hip/abs/chest/shoulder_l/upperarm_l/lowerarm_l/hand_l/ring_01_l/ring_02_l
    m_Weight: 1
  - m_Path: Armature/hip/abs/chest/shoulder_l/upperarm_l/lowerarm_l/hand_l/thumb_01_l
    m_Weight: 1
  - m_Path: Armature/hip/abs/chest/shoulder_l/upperarm_l/lowerarm_l/hand_l/thumb_01_l/thumb_02_l
    m_Weight: 1
  - m_Path: Armature/hip/abs/chest/shoulder_r
    m_Weight: 1
  - m_Path: Armature/hip/abs/chest/shoulder_r/upperarm_r
    m_Weight: 1
  - m_Path: Armature/hip/abs/chest/shoulder_r/upperarm_r/lowerarm_r
    m_Weight: 1
  - m_Path: Armature/hip/abs/chest/shoulder_r/upperarm_r/lowerarm_r/hand_r
    m_Weight: 1
  - m_Path: Armature/hip/abs/chest/shoulder_r/upperarm_r/lowerarm_r/hand_r/index_01_r
    m_Weight: 1
  - m_Path: Armature/hip/abs/chest/shoulder_r/upperarm_r/lowerarm_r/hand_r/index_01_r/index_02_r
    m_Weight: 1
  - m_Path: Armature/hip/abs/chest/shoulder_r/upperarm_r/lowerarm_r/hand_r/mid_01_r
    m_Weight: 1
  - m_Path: Armature/hip/abs/chest/shoulder_r/upperarm_r/lowerarm_r/hand_r/mid_01_r/mid_02_r
    m_Weight: 1
  - m_Path: Armature/hip/abs/chest/shoulder_r/upperarm_r/lowerarm_r/hand_r/pink_01_r
    m_Weight: 1
  - m_Path: Armature/hip/abs/chest/shoulder_r/upperarm_r/lowerarm_r/hand_r/pink_01_r/pink_01_r_001
    m_Weight: 1
  - m_Path: Armature/hip/abs/chest/shoulder_r/upperarm_r/lowerarm_r/hand_r/ring_01_r
    m_Weight: 1
  - m_Path: Armature/hip/abs/chest/shoulder_r/upperarm_r/lowerarm_r/hand_r/ring_01_r/ring_02_r
    m_Weight: 1
  - m_Path: Armature/hip/abs/chest/shoulder_r/upperarm_r/lowerarm_r/hand_r/thumb_01_r
    m_Weight: 1
  - m_Path: Armature/hip/abs/chest/shoulder_r/upperarm_r/lowerarm_r/hand_r/thumb_01_r/thumb_02_r
    m_Weight: 1
  - m_Path: Armature/hip/upperleg_l
    m_Weight: 0
  - m_Path: Armature/hip/upperleg_l/lowerleg_l
    m_Weight: 0
  - m_Path: Armature/hip/upperleg_l/lowerleg_l/ankle_l
    m_Weight: 0
  - m_Path: Armature/hip/upperleg_l/lowerleg_l/ankle_l/foot_l
    m_Weight: 0
  - m_Path: Armature/hip/upperleg_r
    m_Weight: 0
  - m_Path: Armature/hip/upperleg_r/lowerleg_r
    m_Weight: 0
  - m_Path: Armature/hip/upperleg_r/lowerleg_r/ankle_r
    m_Weight: 0
  - m_Path: Armature/hip/upperleg_r/lowerleg_r/ankle_r/foot_r
    m_Weight: 0
  - m_Path: body_final
    m_Weight: 0
  - m_Path: coat_final
    m_Weight: 0
  - m_Path: FaceRig
    m_Weight: 0
  - m_Path: FaceRig/Face_Brow_L
    m_Weight: 1
  - m_Path: FaceRig/Face_Brow_L_001
    m_Weight: 1
  - m_Path: FaceRig/Face_Brow_Mid
    m_Weight: 1
  - m_Path: FaceRig/Face_Brow_R_002
    m_Weight: 1
  - m_Path: FaceRig/Face_Brow_R_003
    m_Weight: 1
  - m_Path: FaceRig/Face_Chin
    m_Weight: 1
  - m_Path: FaceRig/Face_Eye_B_L
    m_Weight: 1
  - m_Path: FaceRig/Face_Eye_B_L_001
    m_Weight: 1
  - m_Path: FaceRig/Face_Eye_B_R_002
    m_Weight: 1
  - m_Path: FaceRig/Face_Eye_B_R_003
    m_Weight: 1
  - m_Path: FaceRig/Face_Eye_T_L
    m_Weight: 1
  - m_Path: FaceRig/Face_Eye_T_L_001
    m_Weight: 1
  - m_Path: FaceRig/Face_Eye_T_R_002
    m_Weight: 1
  - m_Path: FaceRig/Face_Eye_T_R_003
    m_Weight: 1
  - m_Path: FaceRig/Face_Lip_B
    m_Weight: 1
  - m_Path: FaceRig/Face_Lip_L
    m_Weight: 1
  - m_Path: FaceRig/Face_Lip_L_001
    m_Weight: 1
  - m_Path: FaceRig/Face_Lip_R_002
    m_Weight: 1
  - m_Path: FaceRig/Face_Lip_R_003
    m_Weight: 1
  - m_Path: FaceRig/Face_Lip_T
    m_Weight: 1
  - m_Path: hair_final
    m_Weight: 0
  - m_Path: hat_final
    m_Weight: 0
